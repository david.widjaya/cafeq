import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Bottom_nav, Nav_header, Searchbox } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity, PermissionsAndroid } from "react-native"
import { values } from 'mobx';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faFileDownload } from "@fortawesome/free-solid-svg-icons"
import RNFetchBlob from 'rn-fetch-blob';
import { CONFIG } from "@utils/config"

const deviceWidth = Dimensions.get("window").width;

export interface ActivityScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const ActivityScreen: React.FunctionComponent<ActivityScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [email, setEmail] = useState("");
    var [password, setPassword] = useState("");
    var [username, setUsername] = useState("");
    var [search, setSearch] = useState("");
    var [tab_active, setTab_active] = useState(1);
    var [historyOrder, setHistoryOrder] = useState([]);
    var [historyLogin, setHistoryLogin] = useState([]);
    var [statusPage, setStatusPage] = useState(""); //admin, user


    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString();
        var split = number_string.split(',');
        var sisa = split[0].length % 3;
        var rupiah = split[0].substr(0, sisa);
        var ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            var separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    const changeTab = async (paramTab) => {
        setSpinner(true);

        if (statusPage == "user") {
            setTab_active(1);
            return;
        }
        setTab_active(paramTab);
        console.log('tab sekarang: ', tab_active);
        // global.tab_active = paramTab;

        // setSpinner(false);
    }

    const checkLogin = async () => {
        var users = await AsyncStorage.getItem('users');

        if (users !== null) {
            users = JSON.parse(users);
            global.bearer_token = users.bearer_token;
            console.log(global.bearer_token);

            if (users.role_type_emp == 0) {
                //admin
                setStatusPage('admin');
            }
            if (users.role_type_emp == 1) {
                //user
                setStatusPage('user');
            }
            console.log(statusPage);
        }
        console.log(users);
    }


    const downloadReport = async () => {
        const android = RNFetchBlob.android;
        console.log("proses downloading ... ");
        // const tempnameFile = toString(result.data.link);
        // var potong1 = tempnameFile.substr(48);
        // const nameFile = tempnameFile.substr(48, (potong1.length - 1));
        // console.log(nameFile);
        console.log(tab_active);
        const { config, fs } = RNFetchBlob
        var type = null;
        if (tab_active == 2) {
            type = 'login';
        }
        if (tab_active == 1) {
            type = 'order';
        }

        console.log('reporttype: ', type);
        var todayDate = new Date().toISOString().slice(0, 10);
        console.log(todayDate);
        RNFetchBlob.config({
            fileCache: true,
            // path: fs.dirs.DownloadDir + '/report-cafeq/reports.pdf',

            addAndroidDownloads: {
                useDownloadManager: true,
                title: 'reports.pdf',
                description: 'Downloading',
                // mime: 'application/pdf',
                mediaScannable: true,
                notification: true,
                path: fs.dirs.DownloadDir + `/report-cafeq/reports-${todayDate}.pdf`,
            }
        })
            .fetch('GET', CONFIG.API_URL + `/download/report/pdf/${type}`, {
                'Content-Type': 'multipart/form-data',
                'bearer_token': global.bearer_token,
            })
            .then((res) => {
                console.log('res....', res);
                console.log("Download Succesfully, the path : " + res.path());
                android.actionViewIntent(res.path(), 'application/pdf')
            })
        setSpinner(false);
    }


    const requestWritePermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: "CafeQ Permission",
                    message:
                        "CafeQ needs access to your files " +
                        "so you can download files.",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            const grantedRead = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                {
                    title: "CafeQ Permission",
                    message:
                        "CafeQ needs access to your files " +
                        "so you can download files.",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED && grantedRead === PermissionsAndroid.RESULTS.GRANTED) {
                downloadReport();
                return true;
            }
        } catch (err) {
            Alert.alert(
                'Save remote Files',
                'Failed to save Files: ' + err.message,
                [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
                { cancelable: false },
            );
        }
    };

    const getHistoryTrans = async () => {
        setSpinner(true);
        let formData = new FormData();

        var result = await rootStore.getHistoryTrans(formData);

        if (result.kind == "ok") {
            console.log('result: ', result.data.order);
            setHistoryOrder(result.data.order);
            console.log('history order: ', historyOrder);

        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }
    const getHistoryLogin = async () => {
        setSpinner(true);
        let formData = new FormData();

        var result = await rootStore.getHistoryLogin(formData);

        if (result.kind == "ok") {
            console.log('result: ', result.data.history);
            setHistoryLogin(result.data.history);
            console.log('history login: ', historyLogin);
            // const profile = result.data;
            // setAddress(profile.address_emp);
            // setPhone(profile.phone_emp + "");
            // var token = result.data.bearer_token;
            // global.users = result.data;
            // global.bearer_token = token;

            // console.log("Token : " + global.bearer_token);

            // await AsyncStorage.setItem('bearer_token', token);
            // await AsyncStorage.setItem('users', JSON.stringify(result.data));

            // props.navigation.replace("home");
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    useEffect(() => {
        checkLogin();
        setTab_active(global.tab_active);
        getHistoryTrans();
        getHistoryLogin();
    }, [useIsFocused]);


    const _renderTransaction = () => {
        if (tab_active == 1) {
            return (
                <View>
                    <Searchbox
                        title={""}
                        value={search}
                        editable={true}
                        keyboardType={"default"}
                        change={values => setSearch(values)}
                        onPress={() => Alert.alert('hai')}
                        placeholder={'Find your transaction...'}
                    />
                    {
                        historyOrder.map((item, index) => {
                            return (
                                <TouchableOpacity
                                onPress={() => props.navigation.navigate('order_detail', {idorder : item.id_order})}
                                style={{
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                    borderBottomWidth: 1,
                                    paddingVertical: 0.03 * deviceWidth
                                }}>
                                    <Text style={{
                                        ...MainStyle.font_16
                                    }}>
                                        {item.created_at + ""}
                                    </Text>

                                    <View style={{
                                        height: 0.02 * deviceWidth
                                    }} />
                                    <View style={{
                                        flexDirection: 'row',
                                    }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{
                                                width: 0.6 * deviceWidth,
                                                ...MainStyle.font_16,
                                                fontFamily: 'Cabin-SemiBold',
                                            }} >Kode Transaksi: </Text>
                                        <Text
                                            numberOfLines={1}
                                            style={{
                                                width: 0.299 * deviceWidth,
                                                ...MainStyle.font_16,
                                                textAlign: 'right',
                                            }}>{item.id_order}</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                    }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{
                                                width: 0.6 * deviceWidth,
                                                ...MainStyle.font_16,
                                                fontFamily: 'Cabin-SemiBold',
                                            }} >Total </Text>
                                        <Text
                                            numberOfLines={1}
                                            style={{
                                                width: 0.299 * deviceWidth,
                                                ...MainStyle.font_16,
                                                textAlign: 'right',
                                            }}>{formatRupiah(item.total_order + "", 'Rp. ')}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            )
        }
    }

    const _renderLogin = () => {
        if (tab_active == 2) {
            return (
                <View>
                    <Searchbox
                        title={""}
                        value={search}
                        editable={true}
                        keyboardType={"default"}
                        change={values => setSearch(values)}
                        onPress={() => Alert.alert('hai')}
                        placeholder={'Find your last login...'}
                    />

                    {
                        historyLogin.map((item, index) => {
                            return (
                                <TouchableOpacity style={{
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                    borderBottomWidth: 1,
                                    paddingVertical: 0.03 * deviceWidth,
                                }}>
                                    <View style={{
                                        height: 0.02 * deviceWidth
                                    }} />
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                        }} >{(item.name_emp) ? item.name_emp : 'New User!'}</Text>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            ...MainStyle.font_16,
                                        }}>{(item.last_login_at) ? item.last_login_at : 'Not Login Yet'}</Text>
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            )
        }
    }


    return (
        <View style={{ ...Styles.container, alignItems: 'center', position: "relative", }}>
            <Nav_header
                onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Activity"}
            />

            {
                statusPage == "admin" ?
                    <TouchableOpacity
                        onPress={() => requestWritePermission()}
                        style={{
                            position: 'absolute',
                            bottom: 0.4 * deviceWidth,
                            right: 0.05 * deviceWidth,
                            ...Styles.size_40, ...MainStyle.bgcolor_green, borderRadius: 25, justifyContent: 'center', alignItems: 'center',
                            zIndex: 1
                        }}>
                        <FontAwesomeIcon
                            style={{ ...Styles.size_30, color: 'white' }}
                            icon={faFileDownload} />
                    </TouchableOpacity>
                    : <View></View>
            }


            <View
                style={{
                    flexDirection: "row",
                    height: 0.08 * deviceWidth,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >

                <TouchableOpacity
                    onPress={() => { changeTab(1) }}
                    style={{
                        width: 0.44 * deviceWidth,
                        alignItems: "center",
                        flexDirection: "column",
                    }}
                >

                    {(tab_active == 1) ?

                        <View style={{ flexDirection: "column" }}>

                            <Text
                                allowFontScaling={false}
                                numberOfLines={1}
                                style={{
                                    ...MainStyle.font_16,
                                    ...MainStyle.color_black,
                                    marginBottom: 0.01944 * deviceWidth,
                                    alignSelf: "center",
                                }}
                            >Transaction</Text>

                            <View
                                style={{
                                    ...MainStyle.bgcolor_blue,
                                    width: 0.44 * deviceWidth,
                                    height: 0.0027 * deviceWidth,
                                }}
                            />

                        </View>

                        :

                        <Text
                            allowFontScaling={false}
                            numberOfLines={1}
                            style={{
                                ...MainStyle.font_16,
                                ...MainStyle.color_grey_light,
                                marginBottom: 0.01944 * deviceWidth,
                            }}
                        >Transaction</Text>

                    }

                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => { changeTab(2) }}
                    style={{
                        width: 0.44 * deviceWidth,
                        alignItems: "center",
                        flexDirection: "column",
                    }}
                >

                    {(tab_active == 1) ?

                        <Text
                            allowFontScaling={false}
                            numberOfLines={1}
                            style={{
                                ...MainStyle.font_16,
                                ...MainStyle.color_grey_light,
                                marginBottom: 0.01944 * deviceWidth,
                            }}
                        >Login</Text>

                        :

                        <View style={{ flexDirection: "column" }}>

                            <Text
                                allowFontScaling={false}
                                numberOfLines={1}
                                style={{
                                    ...MainStyle.font_16,
                                    ...MainStyle.color_black,
                                    marginBottom: 0.01944 * deviceWidth,
                                    alignSelf: "center",
                                }}
                            >Login</Text>

                            <View
                                style={{
                                    ...MainStyle.bgcolor_blue,
                                    width: 0.44 * deviceWidth,
                                    height: 0.0027 * deviceWidth,
                                }}
                            />

                        </View>

                    }

                </TouchableOpacity>

            </View>





            <ScrollView showsVerticalScrollIndicator={false}>

                {_renderTransaction()}

                {_renderLogin()}

            </ScrollView>

            <Bottom_nav
                page={3}
                home={() => { props.navigation.replace("home") }}
                order={() => { props.navigation.replace("order") }}
                activity={() => { props.navigation.replace("activity") }}
                profile={() => { props.navigation.replace("profile") }}
            />
        </View>
    )
}
