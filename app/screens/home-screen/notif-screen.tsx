import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Bottom_nav, Searchbox, Menuitem, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAdd } from "@fortawesome/free-solid-svg-icons"

const deviceWidth = Dimensions.get("window").width;

export interface NotifScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const NotifScreen: React.FunctionComponent<NotifScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [listNotif, setlistNotif] = useState([]);


    const getNotif = async () => {
        setSpinner(true);

        let formData = new FormData();

        var result = await rootStore.getNotif(formData);
        if (result.kind == "ok") {
            console.log('result: ', result.data);
            setlistNotif(result.data);

        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }

    const markRead = async () => {

    }

    useEffect(() => {
        getNotif();
    }, [useIsFocused]);

    return (
        <View style={{ ...Styles.container, alignItems: 'center', position: 'relative' }}>

            <Nav_header
                onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Notification"}
            />

            <ScrollView showsVerticalScrollIndicator={false}>
                {
                    listNotif.map((item, index) => {
                        return (
                            <View key={index}>
                                {/* <Menuitem
                                    onPress={() => Alert.alert('menu')}
                                    title={item.name_menu}
                                    price={formatRupiah(item.price_menu + "", 'Rp. ')}
                                    editable={true}
                                    id_menu={item.id_menu}
                                /> */}

                                <View style={{
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                    height: 0.3 * deviceWidth,
                                    borderBottomWidth: 1,
                                    borderBottomColor: MainStyle.color_grey_light.color
                                }}>
                                    <Text style={{ ...MainStyle.font_16, }}>{item.created_at}</Text>
                                    <Text style={{ ...MainStyle.font_16, fontWeight: '600', color: 'black' }}>{item.title_notif}</Text>
                                    <Text style={{ ...MainStyle.font_16, }}>{item.desc_notif}</Text>

                                    {/* <View style={{ alignItems:'flex-end' }}>
                                        <View style={{ height: 0.02 * deviceWidth }} />
                                        <View style={{ flexDirection: 'row' }}>
                                            <Button_big
                                                onPress={() => markRead()}
                                                text={'Read'}
                                                size={'small'}
                                            />
                                        </View>

                                    </View> */}
                                </View>
                            </View>
                        )
                    })
                }
                {/* <Button_big
                    onPress={() => props.navigation.replace('login')}
                    text={'Back'}
                /> */}
            </ScrollView>

            <Bottom_nav
                page={1}
                home={() => { props.navigation.replace("home") }}
                order={() => { props.navigation.replace("order") }}
                activity={() => { props.navigation.replace("activity") }}
                profile={() => { props.navigation.replace("profile") }}
            />
        </View>
    )
}
