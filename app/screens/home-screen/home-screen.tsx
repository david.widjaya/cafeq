import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Bottom_nav, Searchbox, Menuitem, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAdd } from "@fortawesome/free-solid-svg-icons"

const deviceWidth = Dimensions.get("window").width;

export interface HomeScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const HomeScreen: React.FunctionComponent<HomeScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [email, setEmail] = useState("");
    var [password, setPassword] = useState("");
    var [search, setSearch] = useState("");
    var [listMenu, setListMenu] = useState([]);
    var [currentId, setCurrentId] = useState(0);
    var [image, setImage] = useState('https://flask.palletsprojects.com/en/2.0.x/_images/debugger.png');
    var [statusPage, setStatusPage] = useState(""); //admin, user


    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        { label: 'Apple', value: 'apple' },
        { label: 'Banana', value: 'banana' }
    ]);
    const [category, setCategory] = useState([]);

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString();
        var split = number_string.split(',');
        var sisa = split[0].length % 3;
        var rupiah = split[0].substr(0, sisa);
        var ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            var separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    const showConfirmDialog = (name, id) => {
        setCurrentId(id);
        console.log('currentID : ', currentId);
        console.log('idsaatini : ', id);

        return Alert.alert(
            "Are your sure?",
            "Are you sure you want to remove this menu with name " + name + "?",
            [
                {
                    text: "Yes",
                    onPress: () => {
                        console.log('cur', currentId);
                        console.log('idd', id);
                        deleteMenu(id);
                    },
                },
                {
                    text: "No",
                },
            ]
        );
    };

    const getCategory = async () => {
        setSpinner(true);

        let formData = new FormData();

        console.log(value);

        var result = await rootStore.getCategory(formData);
        if (result.kind == "ok") {
            console.log('resultcategort: ', result.data.category);
            var arr = [];
            result.data.category.forEach((element, index) => {
                arr.push(
                    {
                        label: element.name_cate_menu,
                        value: element.id_cate_menu
                    }
                );
                console.log(element);
            });
            setCategory(arr);

        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }
    const getListMenu = async () => {
        setSpinner(true);

        let formData = new FormData();

        console.log(value);
        formData.append("q", search);
        if (value != null) {
            formData.append("cate_menu_id", value);
        }

        var result = await rootStore.getListMenu(formData);
        if (result.kind == "ok") {
            console.log('result: ', result.data.menu);
            let tempList = result.data.menu;
            tempList.forEach((element, index) => {
                var tempImage = element.logo_menu;
                console.log(tempImage);
                if(tempImage != null){
                    // let host = await AsyncStorage.getItem('host');
    
                    console.log('host..',global.host);
                    // setImage({uri: global.host + tempImage});
                    element.logo_menu = {uri: global.host + tempImage};
                //   tempImage = {uri: tempImage+"?random="+new Date()};
                }
            });
            setListMenu(tempList);
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }
    const deleteMenu = async (id) => {
        setSpinner(true);

        let formData = new FormData();

        console.log(id);
        formData.append("id_menu", id);

        var result = await rootStore.deleteMenu(formData);
        if (result.kind == "ok") {
            ToastAndroid.show('Data Menu Has Been Deleted Succesfully!', ToastAndroid.SHORT);
            getListMenu();
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }

    const checkLogin = async () => {
        var users = await AsyncStorage.getItem('users');

        if (users !== null) {
            users = JSON.parse(users);
            global.bearer_token = users.bearer_token;
            console.log(global.bearer_token);

            if (users.role_type_emp == 0) {
                //admin
                setStatusPage('admin');
            }
            if (users.role_type_emp == 1) {
                //user
                setStatusPage('user');
            }
            console.log(statusPage);
        }
        console.log(users);
    }

    useEffect(() => {
        checkLogin();
        getListMenu();
        getCategory();
    }, [useIsFocused]);

    return (
        <View style={{ ...Styles.container, alignItems: 'center', position: 'relative' }}>

            <Nav_header
                onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Home"}
            />

            {
                statusPage == "admin" ?
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('menu_add')}
                        style={{
                            position: 'absolute',
                            bottom: 0.3 * deviceWidth,
                            right: 0.05 * deviceWidth,
                            ...Styles.size_40, ...MainStyle.bgcolor_green, borderRadius: 25, justifyContent: 'center', alignItems: 'center',
                            zIndex: 1
                        }}>
                        <FontAwesomeIcon
                            style={{ ...Styles.size_30, color: 'white' }}
                            icon={faAdd} />
                    </TouchableOpacity>
                    : <View></View>
            }

            <Searchbox
                title={""}
                value={search}
                editable={true}
                keyboardType={"default"}
                change={values => setSearch(values)}
                onPress={() => getListMenu()}
                placeholder={'Find your menu...'}
            />

            <View style={{
                marginLeft: 0.055 * deviceWidth,
                marginRight: 0.055 * deviceWidth,
                marginTop: 0.025 * deviceWidth,
                flexDirection: "column",
            }}>
                <Text>{"Filter by Category"}</Text>
                <DropDownPicker
                    open={open}
                    value={value}
                    items={category}
                    setOpen={setOpen}
                    setValue={setValue}
                    setItems={setCategory}
                    style={{
                        ...Styles.dropdown
                    }}
                    dropDownContainerStyle={{
                        ...Styles.dropdown_container
                    }}
                    listMode="SCROLLVIEW"
                    scrollViewProps={{
                        nestedScrollEnabled: true,
                    }}
                />
            </View>

            <ScrollView showsVerticalScrollIndicator={false}>
                {
                    listMenu.map((item, index) => {
                        return (
                            <View key={index}>
                                {/* <Menuitem
                                    onPress={() => Alert.alert('menu')}
                                    title={item.name_menu}
                                    price={formatRupiah(item.price_menu + "", 'Rp. ')}
                                    editable={true}
                                    id_menu={item.id_menu}
                                /> */}

                                <TouchableOpacity style={{
                                    flexDirection: 'row',
                                    height: 0.36 * deviceWidth,
                                    padding: 0.055 * deviceWidth
                                }}
                                    onPress={() => Alert.alert('hai')}
                                >
                                    <View style={{
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 2,
                                        },
                                        shadowOpacity: 0.23,
                                        shadowRadius: 2.62,

                                        elevation: 4,
                                        alignSelf: "center",
                                        borderRadius: 15,
                                    }}>
                                        <Image
                                            style={{
                                                ...Styles.size_120,
                                                borderRadius: 15,
                                            }}
                                            source={ (item.logo_menu == null) ? Images.default_image : item.logo_menu}
                                            // source={ {uri: 'https://6421-202-80-217-206.ngrok.io/api/logo_user/x.png'}}
                                        />
                                    </View>
                                    <View style={{
                                        width: 0.55 * deviceWidth,
                                        marginLeft: 0.05 * deviceWidth
                                    }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{
                                                ...MainStyle.font_16,
                                                fontFamily: 'Cabin-Bold'
                                            }}>
                                            {item.name_menu}
                                        </Text>
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text
                                                numberOfLines={1}
                                                style={{
                                                    ...MainStyle.font_14,
                                                    width: 0.45 * deviceWidth
                                                }}>
                                                {formatRupiah(item.price_menu + "", 'Rp. ')}
                                            </Text>
                                            <Text
                                                numberOfLines={1}
                                                style={{
                                                    ...MainStyle.font_14,
                                                    width: 0.45 * deviceWidth
                                                }}>
                                                {"Stock: " + item.stock_menu}
                                            </Text>
                                        </View>
                                        <View>
                                            <View style={{ height: 0.04 * deviceWidth }} />
                                            {
                                                statusPage == "admin" ?
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Button_big
                                                            onPress={() => props.navigation.navigate("menu_edit", { id_menu: item.id_menu })}
                                                            text={'Edit'}
                                                            size={'small'}
                                                        />
                                                        <Button_big
                                                            onPress={() => showConfirmDialog(item.name_menu, item.id_menu)}
                                                            text={'Delete'}
                                                            size={'small'}
                                                        />
                                                    </View>
                                                    : <View></View>
                                            }

                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    })
                }
                {/* <Button_big
                    onPress={() => props.navigation.replace('login')}
                    text={'Back'}
                /> */}
            </ScrollView>

            <Bottom_nav
                page={1}
                home={() => { props.navigation.replace("home") }}
                order={() => { props.navigation.replace("order") }}
                activity={() => { props.navigation.replace("activity") }}
                profile={() => { props.navigation.replace("profile") }}
            />
        </View>
    )
}
