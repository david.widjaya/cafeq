import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
// import ImagePicker from 'react-native-image-picker';
// import * as ImagePicker from "react-native-image-picker";
import {launchImageLibrary} from 'react-native-image-picker';

const deviceWidth = Dimensions.get("window").width;

export interface MenuEditScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const MenuEditScreen: React.FunctionComponent<MenuEditScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [phone, setPhone] = useState(null);
    var [address, setAddress] = useState(null);


    var [id, setId] = useState(props.route.params.id_menu);
    var [name, setName] = useState(null);
    var [desc, setDesc] = useState(null);
    var [stock, setStock] = useState(null);
    var [price, setPrice] = useState(null);
    var [cate, setCate] = useState(null);
    var [listCategory, setlistCategory] = useState([]);

    var [image, setImage] = useState(null);
    var [checkPic, setCheckpic] = useState(0);
  


    const [open, setOpen] = useState(false);
    const [items, setItems] = useState([
        { label: 'Active', value: 'active' },
        { label: 'Nonactive', value: 'nonactive' }
    ]);

    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    const getMenu = async () => {
        setSpinner(true);
        let formData = new FormData();
        formData.append('id_menu', id);
        var result = await rootStore.getMenu(formData);

        if (result.kind == "ok") {
            console.log('result menux: ', result.data.menu);
            setName(result.data.menu[0].name_menu);
            setDesc(result.data.menu[0].desc_menu);
            setStock(result.data.menu[0].stock_menu+"");
            setPrice(result.data.menu[0].price_menu+"");
            
            var tempImage = result.data.menu[0].logo_menu;
            console.log(tempImage);
            if(tempImage != null){
                let host = await AsyncStorage.getItem('host');

                console.log('host..',global.host);
                setImage({uri: global.host + tempImage});
            //   tempImage = {uri: tempImage+"?random="+new Date()};
            }
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        setSpinner(false);
    }

    const getCategory = async () => {
        setSpinner(true);
        let formData = new FormData();
        var result = await rootStore.getCategory(formData);

        if (result.kind == "ok") {
            console.log('result category: ', result.data.category);
            const temp = result.data.category;
            let arr = [];
            temp.forEach( (item, index) => {
                arr.push({
                    label: item.name_cate_menu,
                    value: item.id_cate_menu,
                });
            });
            setlistCategory(arr);
            console.log('qwdqwd',listCategory);
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        setSpinner(false);
    }

    const updateMenu = async () => {
        setSpinner(true);

        if(name == null || desc == null || stock == null || price == null || cate == null){
            Alert.alert(
                'Ooops...',
                'All Field Is Required',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
            return;
        }
        if(name == '' || desc == '' || stock == '' || price == '' || cate == ''){
            Alert.alert(
                'Ooops...',
                'All Field Is Required',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
            return;
        }

        let formData = new FormData();
        formData.append('id_menu', id);
        formData.append('name_menu', name);
        formData.append('desc_menu', desc);
        formData.append('stock_menu', stock);
        formData.append('price_menu', price);
        formData.append('cate_menu_id', cate);

        if (checkPic == 1) {
            console.log({
                uri: image.uri,
                name: image.fileName,
                type: (image.type) ? image.type : "image/jpeg"
              });
            formData.append("img", {
              uri: image.uri,
              name: image.fileName,
              type: (image.type) ? image.type : "image/jpeg"
            });
          }

        console.log('ini formdata',formData);
        var result = await rootStore.updateMenu(formData);

        if (result.kind == "ok") {
            ToastAndroid.show('Data Menu Has Been Updated Succesfully!', ToastAndroid.SHORT);
            props.navigation.replace('home');
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        setSpinner(false);
    }
    const options = {
        title: 'Select Avatar',
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
        maxWidth: 500,
        maxHeight: 500,
        quality: 0.5,
        rotation: 360
      };

    const uploadImage = () => {
        launchImageLibrary(options, (response) => {
          if (response.didCancel) {
          } else if (response.error) {
          } else if (response.customButton) {
          } else {
            const source = { path: response.uri, fileName: response.fileName, type: response.type, uri: response.uri };
    
            setImage(source);
            setCheckpic(1);
          }
        });
      }

    useEffect(() => {
        getMenu();
        getCategory();
    }, [useIsFocused])

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>
            <Nav_header
 onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Edit Menu"}
            />
            <TouchableOpacity onPress={() => {uploadImage()}}>
            <Image
                style={{
                    ...Styles.size_120,
                    marginTop: 0.03 * deviceWidth,
                    // marginBottom: 0.1166 * deviceWidth,
                    borderRadius: 100,
                    alignSelf: "center",
                }}
                source={(image == null) ? Images.default_image : image}
                // source={Images.default_image}
            />

            </TouchableOpacity>

            <View style={{ flexDirection: 'column' }}>
                <View style={{ height: 0.02 * deviceWidth }} />
                <View>
                    <Textbox
                        title={"Nama Menu"}
                        value={name}
                        editable={true}
                        placeholder={"Nama Menu"}
                        keyboardType={"default"}
                        change={values => setName(values)}
                    />
                    <Textbox
                        title={"Deskripsi"}
                        value={desc}
                        editable={true}
                        placeholder={"Deskripsi"}
                        keyboardType={"default"}
                        change={values => setDesc(values)}
                    />
                    <Textbox
                        title={"Stock"}
                        value={stock}
                        editable={true}
                        placeholder={"Stock"}
                        keyboardType={"default"}
                        change={values => setStock(values)}
                    />
                    <Textbox
                        title={"Harga"}
                        value={price}
                        editable={true}
                        placeholder={"Harga"}
                        keyboardType={"default"}
                        change={values => setPrice(values)}
                    />

                    <View style={{
                        marginLeft: 0.055 * deviceWidth,
                        marginRight: 0.055 * deviceWidth,
                        marginTop: 0.025 * deviceWidth,
                        flexDirection: "column",
                    }}>
                        <Text>{"Category"}</Text>
                        <DropDownPicker
                            open={open}
                            value={cate}
                            items={listCategory}
                            setOpen={setOpen}
                            setValue={setCate}
                            setItems={setlistCategory}
                            style={{
                                ...Styles.dropdown
                            }}
                            dropDownContainerStyle={{
                                ...Styles.dropdown_container
                            }}
                            listMode="SCROLLVIEW"
                            scrollViewProps={{
                                nestedScrollEnabled: true,
                            }}
                        />
                    </View>

                    <View style={{ height: 0.02 * deviceWidth }} />
                    <Button_big
                        onPress={() => updateMenu()}
                        text={'Update'}
                    />
                </View>
            </View>
        </View>
    )
}
