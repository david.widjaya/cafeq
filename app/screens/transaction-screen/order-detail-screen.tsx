import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Bottom_nav, Menucart, Nav_header, Menuitem, PushNotifications } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';

const deviceWidth = Dimensions.get("window").width;

export interface OrderDetailScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const OrderDetailScreen: React.FunctionComponent<OrderDetailScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [idorder, setidorder] = useState(props.route.params.idorder);
    var [order, setorder] = useState(null);
    var [orderDetail, setorderDetail] = useState(null);


    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString();
        var split = number_string.split(',');
        var sisa = split[0].length % 3;
        var rupiah = split[0].substr(0, sisa);
        var ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            var separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }


    const getDetailOrder = async () => {
        setSpinner(true);
        console.log('fetcing......... ');

        let formData = new FormData();

        formData.append("id_order", idorder + "");

        var result = await rootStore.getDetailOrder(formData);
        if (result.kind == "ok") {
            console.log('result: ', result.data);
            console.log('@resultorder: ', result.data.order);
            console.log('@resultorderDetail: ', result.data.orderdetail);
            setorder(result.data.order);
            let temp = result.data.orderdetail;
            temp.forEach((element, index) => {
                element.logo_menu = {uri: global.host + element.logo_menu};
            });
            setorderDetail(temp);
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }

    useEffect(() => {
        getDetailOrder();
    }, []);

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>

            <Nav_header
                onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Order Detail"}
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View>
                    <Textbox
                        title={"Customer"}
                        value={order !== null ? order.customer_name : "-"}
                        editable={false}
                        placeholder={"Your Customer Name"}
                        keyboardType={"default"}
                    />
                    {
                        orderDetail !== null ? orderDetail.map((item, index) => {
                            return (
                                <View>
                                    <Menuitem
                                        onPress={() => Alert.alert('menu')}
                                        title={item.name_menu ? item.name_menu+"" : "-"}
                                        price={item.price_menu ? formatRupiah(item.price_menu + "", 'Rp. ') : "-"}
                                        qty={item.qty_order_detail ? item.qty_order_detail+"" : ""}
                                        image={(item.logo_menu == null) ? Images.default_image : item.logo_menu }
                                    />
                                </View>
                            )
                        })
                        : <View></View>

                    }
                    <Textbox
                        title={"Kode Promo (Opsional)"}
                        value={ order ? order.promo_code : "-"}
                        editable={false}
                        placeholder={"Kode Promo"}
                        keyboardType={"default"}
                    />
                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 0.055 * deviceWidth,
                        marginRight: 0.055 * deviceWidth,
                    }}>
                        <Text
                            numberOfLines={1}
                            style={{
                                width: 0.6 * deviceWidth,
                                ...MainStyle.font_16,
                                fontFamily: 'Cabin-SemiBold',
                            }} >Total Pembayaran</Text>
                        <Text
                            numberOfLines={1}
                            style={{
                                width: 0.299 * deviceWidth,
                                ...MainStyle.font_16,
                                fontFamily: 'Cabin-SemiBold',
                                textAlign: 'right',
                            }}>{ order ? formatRupiah(order.total_order + "", 'Rp. ') : "-"}</Text>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 0.055 * deviceWidth,
                        marginRight: 0.055 * deviceWidth,
                    }}>
                        <Text
                            numberOfLines={1}
                            style={{
                                width: 0.6 * deviceWidth,
                                ...MainStyle.font_16,
                                fontFamily: 'Cabin-SemiBold',
                            }} >Dibayar</Text>
                        <Text
                            numberOfLines={1}
                            style={{
                                width: 0.299 * deviceWidth,
                                ...MainStyle.font_16,
                                fontFamily: 'Cabin-SemiBold',
                                textAlign: 'right',
                            }}>{order ? formatRupiah(order.payment_order + "", 'Rp. ') : "-"}</Text>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 0.055 * deviceWidth,
                        marginRight: 0.055 * deviceWidth,
                    }}>
                        <Text
                            numberOfLines={1}
                            style={{
                                width: 0.6 * deviceWidth,
                                ...MainStyle.font_16,
                                fontFamily: 'Cabin-SemiBold',
                            }} >Kembalian</Text>
                        <Text
                            numberOfLines={1}
                            style={{
                                width: 0.299 * deviceWidth,
                                ...MainStyle.font_16,
                                fontFamily: 'Cabin-SemiBold',
                                textAlign: 'right',
                            }}>{order ? formatRupiah(order.changes_order + "", 'Rp. ')  : "-"}</Text>
                    </View>
                </View>
            </ScrollView>

            <View style={{
                padding: 0.02 * deviceWidth
            }}>

                <Button_big
                    onPress={() => props.navigation.goBack()}
                    text={'Back'}
                />
            </View>

            <Bottom_nav
                page={2}
                home={() => { props.navigation.replace("home") }}
                order={() => { props.navigation.replace("order") }}
                activity={() => { props.navigation.replace("activity") }}
                profile={() => { props.navigation.replace("profile") }}
            />
        </View>
    )
}
