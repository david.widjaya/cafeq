import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Bottom_nav, Menucart, Nav_header, Menuitem, PushNotifications } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import RNFetchBlob from 'rn-fetch-blob';
import { CONFIG } from "@utils/config"


const deviceWidth = Dimensions.get("window").width;

export interface OrderScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const OrderScreen: React.FunctionComponent<OrderScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [email, setEmail] = useState("");
    var [password, setPassword] = useState("");
    var [username, setUsername] = useState("");
    var [promoCode, setPromoCode] = useState("");
    var [orderStatus, setorderStatus] = useState(1);
    var [listMenu, setListMenu] = useState([]);
    var [total, setTotal] = useState(0);
    var [beforetotal, setbeforetotal] = useState(0);
    var [payment, setPayment] = useState(0);
    var [changes, setChanges] = useState(0);
    var [order, setOrder] = useState([]);
    var [event, setEvent] = useState([]);
    var [diskon, setdiskon] = useState(0);
    var [eventID, setEventID] = useState(0);


    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString();
        var split = number_string.split(',');
        var sisa = split[0].length % 3;
        var rupiah = split[0].substr(0, sisa);
        var ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            var separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    const showConfirmDialog = () => {
        return Alert.alert(
            "Are your sure?",
            "Are you sure you want to add this order ? \n Please check your order again, this order cant be cancelled.",
            [
                {
                    text: "Yes",
                    onPress: () => {
                        storeOrder();
                    },
                },
                {
                    text: "No",
                },
            ]
        );
    };

    const getListMenu = async () => {
        setSpinner(true);

        let formData = new FormData();

        // formData.append("q", search);

        var result = await rootStore.getListMenu(formData);
        if (result.kind == "ok") {
            console.log('result: ', result.data.menu);
            let arr = result.data.menu;
            arr.map((item, index) => {
                var tempImage = item.logo_menu;
                item.count = 0;
                item.subtotal = 0;
                if(tempImage != null){
                    item.logo_menu = {uri: global.host + tempImage};
                }

            });
            setListMenu(arr);
            console.log(listMenu);
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }

    const getEvent = async () => {
        setSpinner(true);
        let formData = new FormData();

        formData.append("promo_code", promoCode);
        formData.append("total", beforetotal+"");
        console.log(formData);

        var result = await rootStore.getEvent(formData);
        if (result.kind == "ok") {
            console.log('result PROMO APPLIED: ', result.data);
            setEvent([result.data]);
            setEventID(result.data.id_event);
            let percentagediskon = beforetotal * result.data.discount_event / 100;
            let newtotal = 0;
            if (percentagediskon > result.data.max_discount){
                newtotal = beforetotal - result.data.max_discount;
                setTotal(newtotal);
                setdiskon(result.data.max_discount);
            }else{
                newtotal = beforetotal - percentagediskon;
                setTotal(newtotal);
                setdiskon(percentagediskon);
            }
            // setTotal(  )
            Alert.alert(
                'Ooops...',
                'Kode Promo Untuk Event ' + result.data.name_event + ' Berhasil diterapkan!',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
            // console.log('event promo applied: ', event);
            // console.log(event[0].promo_code);
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    const storeOrder = async () => {
        let temp = [];

        if (payment == null || payment == 0) {
            ToastAndroid.show('Add your payment first!', ToastAndroid.SHORT);
            return;
        }
        if ((payment - total) < 0) {
            ToastAndroid.show('Your payment less than total!', ToastAndroid.SHORT);
            return;
        }
        console.log('listMenuforeach:', listMenu);

        listMenu.forEach((item, index) => {
            if (item.count > 0) {
                let obj = {
                    'menu_id': item.id_menu,
                    'qty_order_detail': item.count,
                    'price_order': item.price_menu,
                    'subtotal_order_detail': item.subtotal,
                }
                temp.push(obj);
            }
            console.log('initempARROBJ:', temp);
        });
        // setOrder(temp);
        // setTimeout(
        //     function() {
        //         console.log('store:', order);
        //     }
        //     .bind(this),
        //     1000
        //   );


        //post api
        setSpinner(true);

        
        let formData = new FormData();
        formData.append('promo_code', promoCode);
        formData.append('total_order', total + "");
        formData.append('customer_name', username + "");
        formData.append('payment_order', payment + "");
        formData.append('changes_order', changes + "");
        formData.append('status_order', 1 + "");
        if(eventID !== 0){
            formData.append('event_id', eventID+"");
        }
        console.log('orderdetail:', temp);
        formData.append('order_detail', JSON.stringify(temp));
        console.log('formData:', formData);
        var result = await rootStore.storeOrder(formData);
        let tempidorder = 0;
        if (result.kind == "ok") {
            console.log('statusorder: ', result);
            if (result.data != null){
                tempidorder = result.data ? result.data.id_order : 0;
                if ( 'title_notif' in result.data){
                    //stock abis
                    console.log('stock abis', result.data.desc_notif);
                    PushNotifications(result.data.title_notif, result.data.desc_notif);
                }
            }

            Alert.alert(
                'Success',
                'Data Order Has Been Created!',
                [
                    { text: 'OK', onPress: () => fetchBill(tempidorder) }
                ],
                { cancelable: false }
            );

            // const profile = result.data;
            // setAddress(profile.address_emp);
            // setPhone(profile.phone_emp + "");
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    const fetchBill = async (idorder) => {
        const android = RNFetchBlob.android;
        console.log("proses downloading ... ");
        // const tempnameFile = toString(result.data.link);
        // var potong1 = tempnameFile.substr(48);
        // const nameFile = tempnameFile.substr(48, (potong1.length - 1));
        // console.log(nameFile);
        const { config, fs } = RNFetchBlob

        var todayDate = new Date().toISOString().slice(0, 10);
        console.log(todayDate);
        RNFetchBlob.config({
            fileCache: true,
            // path: fs.dirs.DownloadDir + '/report-cafeq/reports.pdf',

            addAndroidDownloads: {
                useDownloadManager: true,
                title: 'bill.pdf',
                description: 'Downloading',
                // mime: 'application/pdf',
                mediaScannable: true,
                notification: true,
                path: fs.dirs.DownloadDir + `/bill-cafeq/bills-${todayDate}.pdf`,
            }
        })
            .fetch('GET', CONFIG.API_URL + `/download/bill/${idorder}`, {
                'Content-Type': 'multipart/form-data',
                'bearer_token': global.bearer_token,
            })
            .then((res) => {
                console.log('res....', res);
                console.log("Download Succesfully, the path : " + res.path());
                android.actionViewIntent(res.path(), 'application/pdf')
            })
        setSpinner(false);
    }

    const updateCart = async (cart, type) => {
        const temp = [...listMenu];
        setTotal(0);
        setbeforetotal(0);
        let tempp = 0;
        // temp.filter((item, index) => {
        //     if (item === cart) {
        //         if (type == '-' && item.count == 0) {
        //             return
        //         }

        //         item.count = (type == '+') ? item.count + 1 : item.count - 1;
        //         item.subtotal = item.count * item.price_menu;
        //         console.log('subtotal', item.subtotal);
        //         // console.log('total before...', beforetotal);
        //         tempp = tempp + item.subtotal;
        //     }
        //     return item === cart
        // });

        temp.forEach((item, index) => {
            if (item === cart) {
                if (type == '-' && item.count == 0) {
                    return
                }
                if ( type == '+' && (item.count + 1) > item.stock_menu){
                    Alert.alert('Warning', 'Order Quantity More Than Stock!');
                    tempp = tempp + item.subtotal;
                    console.log('total pembayaran sementara: ' + tempp);
                    return
                }
                item.count = (type == '+') ? item.count + 1 : item.count - 1;
                item.subtotal = item.count * item.price_menu;
                console.log('subtotal', item.subtotal);
                // console.log('total before...', beforetotal);
            }
            tempp = tempp + item.subtotal;
            console.log('total pembayaran sementara: ' + tempp);
        });
        setTotal(tempp);
        setbeforetotal(tempp);
        console.log('total updated...', beforetotal);

        setListMenu(temp);
        console.log(temp);
    }

    useEffect(() => {
        getListMenu();
    }, [useIsFocused]);

    useEffect(() => {
        setChanges(((payment - total) <= 0) ? 0 : payment - total);
    }, [payment]);

    // const nextPage = () => {
    // }

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>

            <Nav_header
 onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Order"}
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                {
                    (orderStatus == 1) ?
                        <View>
                            <Textbox
                                title={"Customer"}
                                value={username}
                                editable={true}
                                placeholder={"Your Customer Name"}
                                keyboardType={"default"}
                                change={values => setUsername(values)}
                            />
                        </View>
                        :
                        (orderStatus == 2) ?
                            listMenu.map((item, index) => {
                                return (
                                    <View>
                                        <Menucart
                                            title={item.name_menu}
                                            price={formatRupiah(item.price_menu + "", 'Rp. ')}
                                            count={item.count}
                                            stock={"Stock : " + item.stock_menu}
                                            onPressLess={() => updateCart(item, '-')}
                                            onPressMore={() => updateCart(item, '+')}
                                            image={(item.logo_menu == null) ? Images.default_image : item.logo_menu}
                                        />
                                    </View>
                                )
                            })
                            :
                            <View>
                                <Textbox
                                    title={"Customer"}
                                    value={username}
                                    editable={false}
                                    placeholder={"Your Customer Name"}
                                    keyboardType={"default"}
                                />
                                {
                                    listMenu.map((item, index) => {
                                        if (item.count > 0) {
                                            return (
                                                <View>
                                                    <Menuitem
                                                        onPress={() => Alert.alert('menu')}
                                                        title={item.name_menu}
                                                        price={formatRupiah(item.price_menu + "", 'Rp. ')}
                                                        qty={item.count}
                                                        image={(item.logo_menu == null) ? Images.default_image : item.logo_menu}
                                                    />
                                                </View>
                                            )
                                        }
                                    })

                                }
                                <Textbox
                                    title={"Kode Promo (Opsional)"}
                                    value={promoCode}
                                    editable={true}
                                    placeholder={"Kode Promo"}
                                    keyboardType={"default"}
                                    change={values => setPromoCode(values)}
                                />
                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 0.045 * deviceWidth,
                                    marginRight: 0.045 * deviceWidth,
                                }}>
                                    <Button_big
                                        onPress={ () => getEvent()}
                                        text={'Apply'}
                                        size={'small'}
                                    />
                                </View>
                                <Textbox
                                    title={"Pembayaran"}
                                    value={payment}
                                    editable={true}
                                    placeholder={"Masukkan Jumlah Pembayaran"}
                                    keyboardType={"default"}
                                    change={values => setPayment(values)}
                                />

                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                }}>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.6 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                        }} >Total</Text>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.299 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                            textAlign: 'right',
                                        }}>{formatRupiah(beforetotal + "", 'Rp. ')}</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                }}>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.6 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                        }} >Potongan Diskon</Text>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.299 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                            textAlign: 'right',
                                        }}>{formatRupiah(diskon + "", 'Rp. -')}</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                }}>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.6 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                        }} >Total Pembayaran</Text>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.299 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                            textAlign: 'right',
                                        }}>{formatRupiah(total + "", 'Rp. ')}</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                }}>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.6 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                        }} >Dibayar</Text>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.299 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                            textAlign: 'right',
                                        }}>{formatRupiah(payment + "", 'Rp. ')}</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                }}>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.6 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                        }} >Kembalian</Text>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            width: 0.299 * deviceWidth,
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                            textAlign: 'right',
                                        }}>{formatRupiah(changes + "", 'Rp. ')}</Text>
                                </View>
                            </View>
                }
            </ScrollView>

            <View style={{
                padding: 0.02 * deviceWidth
            }}>
                <Button_big
                    onPress={() => (orderStatus != 3) ? setorderStatus(orderStatus + 1) : showConfirmDialog()}
                    text={(orderStatus == 1 || orderStatus == 2) ? 'Next' : 'Checkout'}
                />
                <View style={{
                    height: 0.02 * deviceWidth
                }} />

                <Button_big
                    onPress={() => (orderStatus != 1) ? setorderStatus(orderStatus - 1) : Alert.alert('Success', 'Already First Page!')}
                    text={'Back'}
                />
            </View>

            <Bottom_nav
                page={2}
                home={() => { props.navigation.replace("home") }}
                order={() => { props.navigation.replace("order") }}
                activity={() => { props.navigation.replace("activity") }}
                profile={() => { props.navigation.replace("profile") }}
            />
        </View>
    )
}
