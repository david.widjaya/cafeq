import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from '@react-native-community/datetimepicker';

const deviceWidth = Dimensions.get("window").width;

export interface CategoryAddScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const CategoryAddScreen: React.FunctionComponent<CategoryAddScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [name, setName] = useState(null);
    var [desc, setDesc] = useState(null);

    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])


    const addCategory = async () => {
        setSpinner(true);

        if (name == null || desc == null) {
            Alert.alert(
                'Ooops...',
                'All Field Is Required',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        if (name == null || desc == null) {
            Alert.alert(
                'Ooops...',
                'All Field Is Required',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        let formData = new FormData();
        formData.append('name_cate_menu', name);
        formData.append('desc_cate_menu', desc);
        console.log(formData);
        var result = await rootStore.addCategory(formData);

        if (result.kind == "ok") {
            ToastAndroid.show('Data Category Has Been Created Succesfully!', ToastAndroid.SHORT);
            props.navigation.replace('profile');
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        setSpinner(false);
    }

    useEffect(() => {
    }, [useIsFocused])

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>
            <Nav_header
 onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Add Category"}
            />

            <ScrollView showsVerticalScrollIndicator={false} style={{ flexDirection: 'column' }}>
                <View style={{ height: 0.02 * deviceWidth }} />
                <View>
                    <Textbox
                        title={"Nama Category"}
                        value={name}
                        editable={true}
                        placeholder={"Nama Category"}
                        keyboardType={"default"}
                        change={values => setName(values)}
                    />
                    <Textbox
                        title={"Deskripsi"}
                        value={desc}
                        editable={true}
                        placeholder={"Deskripsi"}
                        keyboardType={"default"}
                        change={values => setDesc(values)}
                    />

                    <View style={{ height: 0.02 * deviceWidth }} />
                    <Button_big
                        onPress={() => addCategory()}
                        text={'Add'}
                    />
                </View>
            </ScrollView>
        </View>
    )
}
