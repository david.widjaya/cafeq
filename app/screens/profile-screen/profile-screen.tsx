import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Bottom_nav, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faShop, faReceipt, faListCheck, faEdit, faRemove, faAdd, faUser, faSignOut } from "@fortawesome/free-solid-svg-icons"
import { async } from 'validate.js';

const deviceWidth = Dimensions.get("window").width;

export interface ProfileScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const ProfileScreen: React.FunctionComponent<ProfileScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [email, setEmail] = useState("");
    var [password, setPassword] = useState("");
    var [username, setUsername] = useState("");
    var [nama, setNama] = useState("");
    var [address, setAddress] = useState(null);
    var [phone, setPhone] = useState(0);
    var [editable, setEditable] = useState(false);
    var [statusPage, setStatusPage] = useState('admin'); //tambah, admin, user
    var [image, setImage] = useState(null);
    var [checkPic, setCheckpic] = useState(0);

    // var [statusPage, setStatusPage] = useState( ( global.users.role_type_emp == 0) ? 'admin' : 'user' ); //tambah, admin, user

    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    const checkLogin = async () => {
        var users = await AsyncStorage.getItem('users');

        if (users !== null) {
            users = JSON.parse(users);
            global.bearer_token = users.bearer_token;
            console.log(global.bearer_token);

            if (users.role_type_emp == 0) {
                //admin
                setStatusPage('admin');
            }
            if (users.role_type_emp == 1) {
                //user
                setStatusPage('user');
            }
            console.log(statusPage);
        }
        console.log(users);
    }

    const editProfile = async () => {
        editable ? setEditable(false) : setEditable(true);

        if (editable) {
            //sudah diedit
            if(address == null || phone == null){
                Alert.alert(
                    'Ooops...',
                    'All field is required',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') }
                    ],
                    { cancelable: false }
                );
                return;
            }
            setSpinner(true);
            let formData = new FormData();
            console.log('editing....');
            formData.append('address_emp', address);
            formData.append('phone_emp', phone+"");
            var result = await rootStore.updateProfile(formData);

            if (result.kind == "ok") {
                console.log('status: ', result);
                const profile = result.data;
                setAddress(profile.address_emp);
                setPhone(profile.phone_emp);

                Alert.alert(
                    'Success',
                    'Data User Has Been Updated',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') }
                    ],
                    { cancelable: false }
                );
            }
            else if (result.kind == 'wrong') {
                setSpinner(false);
                Alert.alert(
                    'Ooops...',
                    result.message.toString(),
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') }
                    ],
                    { cancelable: false }
                );
            }

            setSpinner(false);
        }
    }

    const getProfile = async () => {
        setSpinner(true);
        let formData = new FormData();

        var result = await rootStore.getProfile(formData);

        if (result.kind == "ok") {
            console.log('getprof: ', result);
            const profile = result.data;
            setNama(profile.name_emp);
            setAddress(profile.address_emp);
            if (profile.phone_emp !== null){
                setPhone(profile.phone_emp);
            }
            var tempImage = profile.logo_emp;
            console.log(tempImage);
            if(tempImage != null){
                console.log('host..',global.host);
                setImage({uri: global.host + tempImage});
            //   tempImage = {uri: tempImage+"?random="+new Date()};
            }
            // var token = result.data.bearer_token;
            // global.users = result.data;
            // global.bearer_token = token;

            // console.log("Token : " + global.bearer_token);

            // await AsyncStorage.setItem('bearer_token', token);
            // await AsyncStorage.setItem('users', JSON.stringify(result.data));

            // props.navigation.replace("home");
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    useEffect(() => {
        checkLogin();
        getProfile();
    }, [useIsFocused]);

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>
            <Nav_header
 onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Akun"}
            />

            <ScrollView showsVerticalScrollIndicator={false}>

                <Image
                    style={{
                        ...Styles.size_120,
                        marginTop: 0.03 * deviceWidth,
                        // marginBottom: 0.1166 * deviceWidth,
                        borderRadius: 100,
                        alignSelf: "center",
                    }}
                    source={(image == null) ? Images.default_image : image}
                />

                <Text
                    numberOfLines={2}
                    style={{
                        ...MainStyle.font_18,
                        ...MainStyle.color_black,
                        textAlign: 'center',
                        paddingHorizontal: 0.2 * deviceWidth,
                        // backgroundColor:'red'
                    }}>
                    {nama}
                </Text>

                <View style={{ height: 0.1 * deviceWidth }} />

                {
                    (statusPage == 'admin') ?
                        <View>
                            <View style={{
                                marginLeft: 0.055 * deviceWidth,
                                marginRight: 0.055 * deviceWidth,
                                flexDirection: "column",
                            }}>
                                <TouchableOpacity style={{
                                    flexDirection: 'row',
                                    width: 0.91 * deviceWidth,
                                    height: 0.11 * deviceWidth,
                                    alignItems: 'center',
                                    borderBottomWidth: 1
                                }}
                                    onPress={() => props.navigation.replace('home')}
                                >
                                    <FontAwesomeIcon
                                        style={{ ...MainStyle.color_green }}
                                        size={28}
                                        icon={faReceipt} />
                                    <Text style={{
                                        ...MainStyle.font_16,
                                        fontFamily: 'Cabin-SemiBold',
                                        marginLeft: 0.05 * deviceWidth,
                                        ...MainStyle.color_black
                                    }}>
                                        Lihat Menu
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                marginLeft: 0.055 * deviceWidth,
                                marginRight: 0.055 * deviceWidth,
                                flexDirection: "column",
                            }}>
                                <TouchableOpacity style={{
                                    flexDirection: 'row',
                                    width: 0.91 * deviceWidth,
                                    height: 0.11 * deviceWidth,
                                    alignItems: 'center',
                                    borderBottomWidth: 1
                                }}
                                    onPress={() => props.navigation.replace('list_category')}
                                >
                                    <FontAwesomeIcon
                                        style={{ ...MainStyle.color_green }}
                                        size={28}
                                        icon={faListCheck} />
                                    <Text style={{
                                        ...MainStyle.font_16,
                                        fontFamily: 'Cabin-SemiBold',
                                        marginLeft: 0.05 * deviceWidth,
                                        ...MainStyle.color_black
                                    }}>
                                        Lihat Category
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                marginLeft: 0.055 * deviceWidth,
                                marginRight: 0.055 * deviceWidth,
                                flexDirection: "column",
                            }}>
                                <TouchableOpacity style={{
                                    flexDirection: 'row',
                                    width: 0.91 * deviceWidth,
                                    height: 0.11 * deviceWidth,
                                    alignItems: 'center',
                                    borderBottomWidth: 1
                                }}
                                    onPress={() => props.navigation.replace('list_event')}
                                >
                                    <FontAwesomeIcon
                                        style={{ ...MainStyle.color_green }}
                                        size={28}
                                        icon={faReceipt} />
                                    <Text style={{
                                        ...MainStyle.font_16,
                                        fontFamily: 'Cabin-SemiBold',
                                        marginLeft: 0.05 * deviceWidth,
                                        ...MainStyle.color_black
                                    }}>
                                        Lihat Event
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                marginLeft: 0.055 * deviceWidth,
                                marginRight: 0.055 * deviceWidth,
                                flexDirection: "column",
                            }}>
                                <TouchableOpacity 
                                onPress={() => props.navigation.navigate('list_user')}
                                style={{
                                    flexDirection: 'row',
                                    width: 0.91 * deviceWidth,
                                    height: 0.11 * deviceWidth,
                                    alignItems: 'center',
                                    borderBottomWidth: 1,
                                }}>
                                    <FontAwesomeIcon
                                        style={{ ...MainStyle.color_green }}
                                        size={28}
                                        icon={faEdit} />
                                    <Text style={{
                                        ...MainStyle.font_16,
                                        fontFamily: 'Cabin-SemiBold',
                                        marginLeft: 0.05 * deviceWidth,
                                        ...MainStyle.color_black
                                    }}>
                                        Edit User
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                marginLeft: 0.055 * deviceWidth,
                                marginRight: 0.055 * deviceWidth,
                                flexDirection: "column",
                            }}>
                                <TouchableOpacity 
                                onPress={() => props.navigation.navigate('list_user')}
                                style={{
                                    flexDirection: 'row',
                                    width: 0.91 * deviceWidth,
                                    height: 0.11 * deviceWidth,
                                    alignItems: 'center',
                                    borderBottomWidth: 1
                                }}>
                                    <FontAwesomeIcon
                                        style={{ ...MainStyle.color_green }}
                                        size={28}
                                        icon={faRemove} />
                                    <Text style={{
                                        ...MainStyle.font_16,
                                        fontFamily: 'Cabin-SemiBold',
                                        marginLeft: 0.05 * deviceWidth,
                                        ...MainStyle.color_black
                                    }}>
                                        Delete User
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                marginLeft: 0.055 * deviceWidth,
                                marginRight: 0.055 * deviceWidth,
                                flexDirection: "column",
                            }}>
                                <TouchableOpacity 
                                onPress={() => props.navigation.navigate('profile_user_add')}
                                style={{
                                    flexDirection: 'row',
                                    width: 0.91 * deviceWidth,
                                    height: 0.11 * deviceWidth,
                                    alignItems: 'center',
                                    borderBottomWidth: 1
                                }}>
                                    <FontAwesomeIcon
                                        style={{ ...MainStyle.color_green }}
                                        size={28}
                                        icon={faAdd} />
                                    <Text style={{
                                        ...MainStyle.font_16,
                                        fontFamily: 'Cabin-SemiBold',
                                        marginLeft: 0.05 * deviceWidth,
                                        ...MainStyle.color_black
                                    }}>
                                        Tambah User
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                marginLeft: 0.055 * deviceWidth,
                                marginRight: 0.055 * deviceWidth,
                                flexDirection: "column",
                            }}>
                                <TouchableOpacity 
                                onPress={() => props.navigation.navigate('profile_edit')}
                                style={{
                                    flexDirection: 'row',
                                    width: 0.91 * deviceWidth,
                                    height: 0.11 * deviceWidth,
                                    alignItems: 'center',
                                    borderBottomWidth: 1
                                }}>
                                    <FontAwesomeIcon
                                        style={{ ...MainStyle.color_green }}
                                        size={28}
                                        icon={faUser} />
                                    <Text style={{
                                        ...MainStyle.font_16,
                                        fontFamily: 'Cabin-SemiBold',
                                        marginLeft: 0.05 * deviceWidth,
                                        ...MainStyle.color_black
                                    }}>
                                        Edit Profile
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                marginLeft: 0.055 * deviceWidth,
                                marginRight: 0.055 * deviceWidth,
                                flexDirection: "column",
                            }}>
                                <TouchableOpacity style={{
                                    flexDirection: 'row',
                                    width: 0.91 * deviceWidth,
                                    height: 0.11 * deviceWidth,
                                    alignItems: 'center',
                                    borderBottomWidth: 1,
                                }}
                                    onPress={() => props.navigation.replace('login')}
                                >
                                    <FontAwesomeIcon
                                        style={{ ...MainStyle.color_green }}
                                        size={28}
                                        icon={faSignOut} />
                                    <Text style={{
                                        ...MainStyle.font_16,
                                        fontFamily: 'Cabin-SemiBold',
                                        marginLeft: 0.05 * deviceWidth,
                                        ...MainStyle.color_black
                                    }}>
                                        Logout
                                    </Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                        :
                        (statusPage == 'user') ?
                            <View>
                                <Textbox
                                    title={"Alamat"}
                                    value={address}
                                    editable={editable}
                                    placeholder={"Alamat"}
                                    keyboardType={"default"}
                                    change={values => setAddress(values)}
                                />

                                <Textbox
                                    title={"Telepon"}
                                    value={phone}
                                    editable={editable}
                                    placeholder={"Telepon"}
                                    keyboardType={"default"}
                                    change={values => setPhone(values)}
                                />

                                <View style={{ height: 0.02 * deviceWidth }} />
                                <Button_big
                                    onPress={() => editProfile()}
                                    text={(editable) ? 'Submit' : 'Edit'}
                                />

                                <View style={{ height: 0.02 * deviceWidth }} />
                                <Button_big
                                    onPress={() => props.navigation.replace('login')}
                                    text={"Logout"}
                                />
                            </View>
                            :
                            (statusPage == 'tambah') ?
                                <View>
                                    <Textbox
                                        title={"Username"}
                                        value={username}
                                        editable={true}
                                        placeholder={"Username"}
                                        keyboardType={"default"}
                                        change={values => setUsername(values)}
                                    />

                                    <Textbox
                                        title={"Password"}
                                        value={password}
                                        editable={true}
                                        placeholder={"Password"}
                                        keyboardType={"default"}
                                        hidden={true}
                                        change={values => setPassword(values)}
                                    />
                                    <Textbox
                                        title={"Nama"}
                                        value={nama}
                                        editable={true}
                                        placeholder={"Nama"}
                                        keyboardType={"default"}
                                        hidden={true}
                                        change={values => setNama(values)}
                                    />

                                    <View style={{ height: 0.02 * deviceWidth }} />
                                    <Button_big
                                        onPress={() => props.navigation.replace('login')}
                                        text={'Submit'}
                                    />
                                </View>
                                : ''
                }
            </ScrollView>

            <Bottom_nav
                page={4}
                home={() => { props.navigation.replace("home") }}
                order={() => { props.navigation.replace("order") }}
                activity={() => { props.navigation.replace("activity") }}
                profile={() => { props.navigation.replace("profile") }}
            />
        </View>
    )
}
