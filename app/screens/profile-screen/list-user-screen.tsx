import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Nav_header, Searchbox } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';

const deviceWidth = Dimensions.get("window").width;

export interface ListUserScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const ListUserScreen: React.FunctionComponent<ListUserScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [phone, setPhone] = useState("0891237123");
    var [password, setPassword] = useState("admin");
    var [search, setSearch] = useState("");
    var [listUser, setListUser] = useState([]);
    var [isDelete, setisDelete] = useState(true);
    var [currentId, setCurrentId] = useState(true);

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        { label: 'Active', value: 'active' },
        { label: 'Nonactive', value: 'nonactive' }
    ]);

    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    const showConfirmDialog = (name, id) => {
        setCurrentId(id);
        return Alert.alert(
          "Are your sure?",
          "Are you sure you want to remove this user with name "+name+"?",
          [
            {
              text: "Yes",
              onPress: () => {
                setisDelete(false);
                console.log('cur', currentId);
                console.log('idd', id);
                deleteUser(id);
              },
            },
            {
              text: "No",
            },
          ]
        );
      };


    const getListUser = async () => {
        setSpinner(true);
        let formData = new FormData();
        formData.append('query', search);

        var result = await rootStore.getListUser(formData);

        if (result.kind == "ok") {
            console.log('result-list: ', result.data.employee);
            setListUser(result.data.employee);
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    const deleteUser = async (id) => {
        setSpinner(true);
        let formData = new FormData();
        formData.append('id_emp', id);
        console.log('id..', id);
        var result = await rootStore.deleteUser(formData);

        if (result.kind == "ok") {
            console.log('result: ', result.data);
            ToastAndroid.show('Data User Has Been Deleted Succesfully!', ToastAndroid.SHORT);
            getListUser();
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    useEffect(() => {
        getListUser();
    }, [useIsFocused])
    

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>
            <Nav_header
 onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Daftar User"}
            />

                <View>
                    <Searchbox
                        title={""}
                        value={search}
                        editable={true}
                        keyboardType={"default"}
                        change={values => setSearch(values)}
                        onPress={() => getListUser()}
                        placeholder={'Find user by username...'}
                    />

                    {
                        listUser.map((item, index) => {
                            return (
                                <TouchableOpacity style={{
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                    borderBottomWidth: 1,
                                    paddingVertical: 0.03 * deviceWidth,
                                    flexDirection:'row',
                                    width:0.9*deviceWidth
                                }}>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            ...MainStyle.font_16,
                                            fontFamily: 'Cabin-SemiBold',
                                            width: 0.4*deviceWidth
                                        }} >{(item.name_emp) ? item.name_emp : 'New User!'}</Text>

                                    <Button_big
                                        onPress={() => props.navigation.navigate('profile_user_edit', { user : item })}
                                        text={'Edit'}
                                        size={'small'}
                                    />
                                    <Button_big
                                        onPress={() => showConfirmDialog(item.name_emp, item.id_emp)}
                                        text={'Delete'}
                                        size={'small'}
                                    />
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>

            {/* <View style={{ flexDirection: 'column' }}>
                <View style={{ height: 0.02 * deviceWidth }} />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <Text>

                        </Text>
                        <Button_big
                            onPress={() => props.navigation.replace('login')}
                            text={'Submit'}
                        />
                    </View>

                </ScrollView>
            </View> */}
        </View>
    )
}
