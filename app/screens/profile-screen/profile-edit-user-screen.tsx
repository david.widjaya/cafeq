import React, { useState, useEffect } from 'react';
import { ParamListBase } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
import {launchImageLibrary} from 'react-native-image-picker';


const deviceWidth = Dimensions.get("window").width;

export interface ProfileEditUserScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const ProfileEditUserScreen: React.FunctionComponent<ProfileEditUserScreenProps> = props => {
    const rootStore = useStores();
    var [spinner, setSpinner] = useState(false);
    var [user, setUser] = useState(props.route.params.user);
    var [phone, setPhone] = useState((props.route.params.user.phone_emp) ? props.route.params.user.phone_emp+"" : "");
    var [password, setPassword] = useState(props.route.params.user.password_emp);

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(props.route.params.user.status);
    const [items, setItems] = useState([
        { label: 'Active', value: 'active' },
        { label: 'Nonactive', value: 'nonactive' }
    ]);

    var [image, setImage] = useState((props.route.params.user.logo_emp) ? {uri: global.host + props.route.params.user.logo_emp}  : null);
    var [checkPic, setCheckpic] = useState(0);


    const options = {
        title: 'Select Avatar',
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
        maxWidth: 500,
        maxHeight: 500,
        quality: 0.5,
        rotation: 360
      };

    const uploadImage = () => {
        console.log('image...', image);
        launchImageLibrary(options, (response) => {
          if (response.didCancel) {
          } else if (response.error) {
          } else if (response.customButton) {
          } else {
            const source = { path: response.uri, fileName: response.fileName, type: response.type, uri: response.uri };
    
            setImage(source);
            setCheckpic(1);
          }
        });
      }

    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    
    const updateUser = async () => {
        console.log(user.phone_emp);
        console.log(phone);
        if(value == null || phone == null || password == null){
            Alert.alert(
                'Ooops...',
                'All field is required',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
            return;
        }

        setSpinner(true);
        let formData = new FormData();
        formData.append('password_emp', password);
        formData.append('phone_emp', phone);
        formData.append('status', value);
        formData.append('emp_id', user.id_emp);
        if (checkPic == 1) {
            console.log({
                uri: image.uri,
                name: image.fileName,
                type: (image.type) ? image.type : "image/jpeg"
              });
            formData.append("img", {
              uri: image.uri,
              name: image.fileName,
              type: (image.type) ? image.type : "image/jpeg"
            });
          }

        var result = await rootStore.updateUser(formData);
        console.log(result.data);
        if (result.kind == "ok") {
            Alert.alert(
                'Ooops...',
                'Data Has Been Updated!',
                [
                    { text: 'OK', onPress: () => props.navigation.replace('list_user') }
                ],
                { cancelable: false }
            );
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>
            <Nav_header
 onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Edit User"}
            />
            <TouchableOpacity onPress={() => {uploadImage()}}>
            <Image
                style={{
                    ...Styles.size_120,
                    marginTop: 0.03 * deviceWidth,
                    // marginBottom: 0.1166 * deviceWidth,
                    borderRadius: 100,
                    alignSelf: "center",
                }}
                source={(image == null) ? Images.default_image : image}
            />
            </TouchableOpacity>

            <Text
                numberOfLines={2}
                style={{
                    ...MainStyle.font_18,
                    ...MainStyle.color_black,
                    textAlign: 'center',
                    paddingHorizontal: 0.2 * deviceWidth,
                    // backgroundColor:'red'
                }}>
                {(user.name_emp) ? user.name_emp : 'New User!'}
            </Text>

            <View style={{ flexDirection: 'column' }}>
                <View style={{ height: 0.02 * deviceWidth }} />
                <View>
                    <Textbox
                        title={"Password"}
                        value={password}
                        editable={true}
                        hidden={true}
                        placeholder={"Password"}
                        keyboardType={"default"}
                        change={values => setPassword(values)}
                    />

                    <View style={{
                        marginLeft: 0.055 * deviceWidth,
                        marginRight: 0.055 * deviceWidth,
                        marginTop: 0.025 * deviceWidth,
                        flexDirection: "column",
                    }}>
                        <Text>{"Select Status"}</Text>
                        <DropDownPicker
                            open={open}
                            value={value}
                            items={items}
                            setOpen={setOpen}
                            setValue={setValue}
                            setItems={setItems}
                            style={{
                                ...Styles.dropdown
                            }}
                            dropDownContainerStyle={{
                                ...Styles.dropdown_container
                            }}
                            listMode="SCROLLVIEW"
                            scrollViewProps={{
                                nestedScrollEnabled: true,
                            }}
                        />
                    </View>

                    <Textbox
                        title={"Telepon"}
                        value={phone}
                        editable={true}
                        placeholder={"Telepon"}
                        keyboardType={"default"}
                        change={values => setPhone(values)}
                    />

                    <View style={{ height: 0.02 * deviceWidth }} />
                    <Button_big
                        onPress={() => updateUser()}
                        text={'Update'}
                    />
                </View>
            </View>
        </View>
    )
}
