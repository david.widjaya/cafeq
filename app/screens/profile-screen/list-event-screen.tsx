import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Bottom_nav, Searchbox, Menuitem } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAdd } from "@fortawesome/free-solid-svg-icons"

const deviceWidth = Dimensions.get("window").width;

export interface ListEventScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const ListEventScreen: React.FunctionComponent<ListEventScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [email, setEmail] = useState("");
    var [password, setPassword] = useState("");
    var [search, setSearch] = useState("");
    var [listEvent, setlistEvent] = useState([]);
    var [currentId, setCurrentId] = useState(true);


    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        { label: 'Apple', value: 'apple' },
        { label: 'Banana', value: 'banana' }
    ]);

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString();
        var split = number_string.split(',');
        var sisa = split[0].length % 3;
        var rupiah = split[0].substr(0, sisa);
        var ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            var separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    const showConfirmDialog = (name, id) => {
        setCurrentId(id);
        return Alert.alert(
            "Are your sure?",
            "Are you sure you want to remove this event with name " + name + "?",
            [
                {
                    text: "Yes",
                    onPress: () => {
                        console.log('cur', currentId);
                        console.log('iddd', id);
                        deleteEvent(id);
                    },
                },
                {
                    text: "No",
                },
            ]
        );
    };

    const getListEvent = async () => {
        setSpinner(true);

        let formData = new FormData();

        formData.append("q", search);

        var result = await rootStore.getListEvent(formData);
        if (result.kind == "ok") {
            console.log('result: ', result.data.event);
            setlistEvent(result.data.event);

        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }
    const deleteEvent = async (id) => {
        setSpinner(true);

        let formData = new FormData();

        console.log(id);
        formData.append("id_event", id);

        var result = await rootStore.deleteEvent(formData);
        if (result.kind == "ok") {
            ToastAndroid.show('Data Event Has Been Deleted Succesfully!', ToastAndroid.SHORT);
            getListEvent();
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }

    useEffect(() => {
        getListEvent();
    }, [useIsFocused]);

    return (
        <View style={{ ...Styles.container, alignItems: 'center', position: 'relative' }}>
            <TouchableOpacity
                onPress={() => props.navigation.navigate('event_add')}
                style={{
                    position: 'absolute',
                    bottom: 0.3 * deviceWidth,
                    right: 0.05 * deviceWidth,
                    ...Styles.size_40, ...MainStyle.bgcolor_green, borderRadius: 25, justifyContent: 'center', alignItems: 'center',
                    zIndex: 1
                }}>
                <FontAwesomeIcon
                    style={{ ...Styles.size_30, color: 'white' }}
                    icon={faAdd} />
            </TouchableOpacity>
            <Searchbox
                title={""}
                value={search}
                editable={true}
                keyboardType={"default"}
                change={values => setSearch(values)}
                onPress={() => getListEvent()}
                placeholder={'Find your event...'}
            />

            <ScrollView showsVerticalScrollIndicator={false}>
                {
                    listEvent.map((item, index) => {
                        return (
                            <View key={index}>
                                {/* <Menuitem
                                    onPress={() => Alert.alert('menu')}
                                    title={item.name_menu}
                                    price={formatRupiah(item.price_menu + "", 'Rp. ')}
                                    editable={true}
                                    id_menu={item.id_menu}
                                /> */}

                                <TouchableOpacity style={{
                                    flexDirection: 'row',
                                    height: 0.4 * deviceWidth,
                                    padding: 0.055 * deviceWidth
                                }}
                                    onPress={() => Alert.alert('hai')}
                                >
                                    <View style={{
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 2,
                                        },
                                        shadowOpacity: 0.23,
                                        shadowRadius: 2.62,

                                        elevation: 4,
                                        alignSelf: "center",
                                        borderRadius: 15,
                                    }}>
                                        <Image
                                            style={{
                                                ...Styles.size_120,
                                                borderRadius: 15,
                                            }}
                                            source={Images.default_food}
                                        />
                                    </View>
                                    <View style={{
                                        width: 0.55 * deviceWidth,
                                        marginLeft: 0.05 * deviceWidth
                                    }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{
                                                ...MainStyle.font_16,
                                                fontFamily: 'Cabin-Bold'
                                            }}>
                                            {item.name_event}
                                        </Text>
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text
                                                numberOfLines={1}
                                                style={{
                                                    ...MainStyle.font_14,
                                                    width: 0.45 * deviceWidth
                                                }}>
                                                {item.promo_code}
                                            </Text>
                                            <Text
                                                numberOfLines={1}
                                                style={{
                                                    ...MainStyle.font_14,
                                                    width: 0.45 * deviceWidth
                                                }}>
                                                {item.discount_event + '% up to ' + formatRupiah(item.max_discount + "", 'Rp. ')}
                                            </Text>
                                        </View>
                                        <View>
                                            <View style={{ height: 0.07 * deviceWidth }} />
                                            <View style={{ flexDirection: 'row' }}>
                                                <Button_big
                                                    onPress={() => showConfirmDialog(item.name_event, item.id_event)}
                                                    text={'Delete'}
                                                    size={'small'}
                                                />
                                            </View>

                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    })
                }
                {/* <Button_big
                    onPress={() => props.navigation.replace('login')}
                    text={'Back'}
                /> */}
            </ScrollView>

            <Bottom_nav
                page={1}
                home={() => { props.navigation.replace("home") }}
                order={() => { props.navigation.replace("order") }}
                activity={() => { props.navigation.replace("activity") }}
                profile={() => { props.navigation.replace("profile") }}
            />
        </View>
    )
}
