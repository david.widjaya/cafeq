import React, { useState, useEffect } from 'react';
import { ParamListBase } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
import {launchImageLibrary} from 'react-native-image-picker';

const deviceWidth = Dimensions.get("window").width;

export interface ProfileAddUserScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const ProfileAddUserScreen: React.FunctionComponent<ProfileAddUserScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [nama, setNama] = useState(null);
    var [username, setUsername] = useState(null);
    var [password, setPassword] = useState(null);

    var [image, setImage] = useState(null);
    var [checkPic, setCheckpic] = useState(0);

    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    const options = {
        title: 'Select Avatar',
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
        maxWidth: 500,
        maxHeight: 500,
        quality: 0.5,
        rotation: 360
      };

    const uploadImage = () => {
        launchImageLibrary(options, (response) => {
          if (response.didCancel) {
          } else if (response.error) {
          } else if (response.customButton) {
          } else {
            const source = { path: response.uri, fileName: response.fileName, type: response.type, uri: response.uri };
    
            setImage(source);
            setCheckpic(1);
          }
        });
      }

    const addUser = async () => {
        if (nama == null || username == null || password == null) {
            Alert.alert('Failed', 'All field is required!');
            return;
        }

        setSpinner(true);
        let formData = new FormData();
        formData.append('username_emp', username);
        formData.append('password_emp', password);
        formData.append('name_emp', nama);
        formData.append('role_type_emp', '1');
        formData.append('status', 'active');
        if (checkPic == 1) {
            console.log({
                uri: image.uri,
                name: image.fileName,
                type: (image.type) ? image.type : "image/jpeg"
              });
            formData.append("img", {
              uri: image.uri,
              name: image.fileName,
              type: (image.type) ? image.type : "image/jpeg"
            });
          }

        var result = await rootStore.addUser(formData);

        if (result.kind == "ok") {
            Alert.alert(
                'Ooops...',
                'Data Employee Has Been Created!',
                [
                    { text: 'OK', onPress: () => props.navigation.replace('profile') }
                ],
                { cancelable: false }
            );
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>
            <Nav_header
 onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Tambah User"}
            />
            <TouchableOpacity onPress={() => {uploadImage()}}>

            <Image
                style={{
                    ...Styles.size_120,
                    marginTop: 0.03 * deviceWidth,
                    // marginBottom: 0.1166 * deviceWidth,
                    borderRadius: 100,
                    alignSelf: "center",
                }}
                source={(image == null) ? Images.default_image : image}
            />
            </TouchableOpacity>

            <View style={{ flexDirection: 'column' }}>
                <View style={{ height: 0.02 * deviceWidth }} />
                <View>
                    <Textbox
                        title={"Username"}
                        value={username}
                        editable={true}
                        placeholder={"Username"}
                        keyboardType={"default"}
                        change={values => setUsername(values)}
                    />

                    <Textbox
                        title={"Password"}
                        value={password}
                        editable={true}
                        hidden={true}
                        placeholder={"Password"}
                        keyboardType={"default"}
                        change={values => setPassword(values)}
                    />

                    <Textbox
                        title={"Nama"}
                        value={nama}
                        editable={true}
                        placeholder={"Nama"}
                        keyboardType={"default"}
                        change={values => setNama(values)}
                    />

                    <View style={{ height: 0.02 * deviceWidth }} />
                    <Button_big
                        onPress={() => addUser()}
                        text={'Add'}
                    />
                </View>
            </View>
        </View>
    )
}
