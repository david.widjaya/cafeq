import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Bottom_nav, Searchbox, Menuitem, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAdd } from "@fortawesome/free-solid-svg-icons"

const deviceWidth = Dimensions.get("window").width;

export interface ListCategoryScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const ListCategoryScreen: React.FunctionComponent<ListCategoryScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [email, setEmail] = useState("");
    var [password, setPassword] = useState("");
    var [search, setSearch] = useState("");
    var [listCategory, setlistCategory] = useState([]);
    var [currentId, setCurrentId] = useState(true);


    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        { label: 'Apple', value: 'apple' },
        { label: 'Banana', value: 'banana' }
    ]);

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString();
        var split = number_string.split(',');
        var sisa = split[0].length % 3;
        var rupiah = split[0].substr(0, sisa);
        var ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            var separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    const showConfirmDialog = (name, id) => {
        setCurrentId(id);
        return Alert.alert(
            "Are your sure?",
            "Are you sure you want to remove this event with name " + name + "?",
            [
                {
                    text: "Yes",
                    onPress: () => {
                        console.log('cur', currentId);
                        console.log('iddd', id);
                        deleteCategory(id);
                    },
                },
                {
                    text: "No",
                },
            ]
        );
    };

    const getListCategory = async () => {
        setSpinner(true);

        let formData = new FormData();

        // formData.append("q", search);

        var result = await rootStore.getCategory(formData);
        if (result.kind == "ok") {
            console.log('result: ', result.data.category);
            setlistCategory(result.data.category);

        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }
    const deleteCategory = async (id) => {
        setSpinner(true);

        let formData = new FormData();

        console.log(id);
        formData.append("id_cate_menu", id);

        var result = await rootStore.deleteCategory(formData);
        if (result.kind == "ok") {
            ToastAndroid.show('Data Event Has Been Deleted Succesfully!', ToastAndroid.SHORT);
            getListCategory();
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        // }

        setSpinner(false);
    }

    useEffect(() => {
        getListCategory();
    }, [useIsFocused]);

    return (
        <View style={{ ...Styles.container, alignItems: 'center', position: 'relative' }}>
            <TouchableOpacity
                onPress={() => props.navigation.navigate('category_add')}
                style={{
                    position: 'absolute',
                    bottom: 0.3 * deviceWidth,
                    right: 0.05 * deviceWidth,
                    ...Styles.size_40, ...MainStyle.bgcolor_green, borderRadius: 25, justifyContent: 'center', alignItems: 'center',
                    zIndex: 1
                }}>
                <FontAwesomeIcon
                    style={{ ...Styles.size_30, color: 'white' }}
                    icon={faAdd} />
            </TouchableOpacity>

            <Nav_header
                back={true}
                title={"Daftar Category"}
            />

            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ height: 0.05 * deviceWidth }} />
                {
                    listCategory.map((item, index) => {
                        return (
                            <View key={index}>
                                <TouchableOpacity style={{
                                    flexDirection: 'row',
                                    height: 0.4 * deviceWidth,
                                    marginLeft: 0.055 * deviceWidth,
                                    marginRight: 0.055 * deviceWidth,
                                }}
                                    onPress={() => Alert.alert('hai')}
                                >
                                    <View style={{
                                        width: 0.9 * deviceWidth,
                                        paddingLeft: 0.055 * deviceWidth,
                                        paddingRight: 0.055 * deviceWidth,
                                    }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{
                                                ...MainStyle.font_16,
                                                fontFamily: 'Cabin-Bold'
                                            }}>
                                            {item.name_cate_menu}
                                        </Text>
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text
                                                numberOfLines={1}
                                                style={{
                                                    ...MainStyle.font_14,
                                                    width: 0.45 * deviceWidth
                                                }}>
                                                {item.desc_cate_menu}
                                            </Text>
                                            <Text
                                                numberOfLines={1}
                                                style={{
                                                    ...MainStyle.font_14,
                                                    width: 0.45 * deviceWidth
                                                }}>
                                                {'Total Item: ' + item.total_cate_menu}
                                            </Text>
                                        </View>
                                        <View>
                                            <View style={{ flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end', }}>
                                                <Button_big
                                                    onPress={() => showConfirmDialog(item.name_cate_menu, item.id_cate_menu)}
                                                    text={'Delete'}
                                                    size={'small'}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    })
                }
                {/* <Button_big
                    onPress={() => props.navigation.replace('login')}
                    text={'Back'}
                /> */}
            </ScrollView>

            <Bottom_nav
                page={1}
                home={() => { props.navigation.replace("home") }}
                order={() => { props.navigation.replace("order") }}
                activity={() => { props.navigation.replace("activity") }}
                profile={() => { props.navigation.replace("profile") }}
            />
        </View>
    )
}
