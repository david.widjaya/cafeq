import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
import {launchImageLibrary} from 'react-native-image-picker';

const deviceWidth = Dimensions.get("window").width;

export interface ProfileEditScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const ProfileEditScreen: React.FunctionComponent<ProfileEditScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [phone, setPhone] = useState(null);
    var [name, setName] = useState(null);
    var [address, setAddress] = useState(null);

    var [image, setImage] = useState(null);
    var [checkPic, setCheckpic] = useState(0);

    const options = {
        title: 'Select Avatar',
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
        maxWidth: 500,
        maxHeight: 500,
        quality: 0.5,
        rotation: 360
      };

    const uploadImage = () => {
        launchImageLibrary(options, (response) => {
          if (response.didCancel) {
          } else if (response.error) {
          } else if (response.customButton) {
          } else {
            const source = { path: response.uri, fileName: response.fileName, type: response.type, uri: response.uri };
    
            setImage(source);
            setCheckpic(1);
          }
        });
      }

    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    const getProfile = async () => {
        setSpinner(true);
        let formData = new FormData();

        var result = await rootStore.getProfile(formData);

        if (result.kind == "ok") {
            console.log('getprof: ', result);
            const profile = result.data;
            setName(profile.name_emp);
            setAddress(profile.address_emp);
            setPhone((profile.phone_emp) ? profile.phone_emp+"" : "");
            var tempImage = profile.logo_emp;
            console.log(tempImage);
            if(tempImage != null){
                console.log('host..',global.host);
                setImage({uri: global.host + tempImage});
            //   tempImage = {uri: tempImage+"?random="+new Date()};
            }

        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    const updateProfile = async () => {
        if(address == '' || phone == '' || address == null || phone == null){
            Alert.alert(
                'Ooops...',
                'All field is required',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
            return;
        }

        setSpinner(true);
        let formData = new FormData();
        console.log('editing....');
        formData.append('address_emp', address);
        formData.append('phone_emp', phone);

        if (checkPic == 1) {
            console.log({
                uri: image.uri,
                name: image.fileName,
                type: (image.type) ? image.type : "image/jpeg"
              });
            formData.append("img", {
              uri: image.uri,
              name: image.fileName,
              type: (image.type) ? image.type : "image/jpeg"
            });
          }

        console.log('ini formdata',formData);

        var result = await rootStore.updateProfile(formData);

        if (result.kind == "ok") {
            console.log('status: ', result);
            Alert.alert(
                'Success',
                'Data User Has Been Updated',
                [
                    { text: 'OK', onPress: () => props.navigation.replace('profile') }
                ],
                { cancelable: false }
            );
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        setSpinner(false);
    }

    useEffect(() => {
        getProfile();
    }, [useIsFocused])

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>
            <Nav_header
 onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Edit Profile"}
            />
            <TouchableOpacity onPress={() => {uploadImage()}}>
            <Image
                style={{
                    ...Styles.size_120,
                    marginTop: 0.03 * deviceWidth,
                    // marginBottom: 0.1166 * deviceWidth,
                    borderRadius: 100,
                    alignSelf: "center",
                }}
                source={(image == null) ? Images.default_image : image}
            />
            </TouchableOpacity>

            <Text
                numberOfLines={2}
                style={{
                    ...MainStyle.font_18,
                    ...MainStyle.color_black,
                    textAlign: 'center',
                    paddingHorizontal: 0.2 * deviceWidth,
                    // backgroundColor:'red'
                }}>
                {name}
            </Text>

            <View style={{ flexDirection: 'column' }}>
                <View style={{ height: 0.02 * deviceWidth }} />
                <View>
                    <Textbox
                        title={"Alamat"}
                        value={address}
                        editable={true}
                        placeholder={"Alamat"}
                        keyboardType={"default"}
                        change={values => setAddress(values)}
                    />

                    <Textbox
                        title={"Telepon"}
                        value={phone}
                        editable={true}
                        placeholder={"Telepon"}
                        keyboardType={"default"}
                        change={values => setPhone(values)}
                    />

                    <View style={{ height: 0.02 * deviceWidth }} />
                    <Button_big
                        onPress={() => updateProfile()}
                        text={'Update'}
                    />
                </View>
            </View>
        </View>
    )
}
