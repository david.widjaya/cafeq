import React, { useState, useEffect } from 'react';
import { ParamListBase, useIsFocused } from "@react-navigation/native"
import { NativeStackNavigationProp } from "react-native-screens/native-stack"
import { useStores } from "@models/root-store"
import Spinner from 'react-native-loading-spinner-overlay';
import { MainStyle, Styles, Images, Helper } from "@theme"
import '../../../global.js'
import { Textbox, Button_big, Nav_header } from "@components"
import { Dimensions, View, Text, ToastAndroid, ScrollView, AsyncStorage, Alert, Image, TextInput, TouchableOpacity } from "react-native"
import { values } from 'mobx';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from '@react-native-community/datetimepicker';

const deviceWidth = Dimensions.get("window").width;

export interface EventAddScreenProps {
    navigation: NativeStackNavigationProp<ParamListBase>
}

export const EventAddScreen: React.FunctionComponent<EventAddScreenProps> = props => {
    const rootStore = useStores();

    var [spinner, setSpinner] = useState(false);
    var [phone, setPhone] = useState(null);
    var [address, setAddress] = useState(null);
    var [show, setShow] = useState(false);
    var [show1, setShow1] = useState(false);


    var [name, setName] = useState(null);
    var [desc, setDesc] = useState(null);
    var [promoCode, setpromoCode] = useState(null);
    var [dateStart, setdateStart] = useState(new Date());
    var [dateStarts, setdateStarts] = useState(null);
    var [dateEnd, setdateEnd] = useState(new Date());
    var [dateEnds, setdateEnds] = useState(null);

    var [minPrice, setminPrice] = useState(null);
    var [discount, setdiscount] = useState(null);
    var [maxDiscount, setmaxDiscount] = useState(null);
    var [listCategory, setlistCategory] = useState([]);


    const [open, setOpen] = useState(false);

    const goBack = React.useMemo(() => () => props.navigation.goBack(), [
        props.navigation,
    ])

    const onChangeDate = (event, selectedDate) => {
        setShow(false);

        if (dateStart !== undefined) {
            const currentDate = (selectedDate.getDate()+1 <= 9) ? '0' + (selectedDate.getDate()+1) : selectedDate.getDate()+1;
            const currentMonth = (selectedDate.getMonth()+1 <= 9) ? '0' + (selectedDate.getMonth()+1) : selectedDate.getMonth()+1;
            const currentYear = selectedDate.getFullYear();
            var tempFinal = currentYear + "-" + currentMonth + "-" + currentDate;

            setdateStart(selectedDate);
            setdateStarts(tempFinal);
        }
    };
    const onChangeDate2 = (event, selectedDate) => {
        setShow1(false);

        if (dateEnd !== undefined) {
            const currentDate = (selectedDate.getDate()+1 <= 9) ? '0' + (selectedDate.getDate()+1) : selectedDate.getDate()+1;
            const currentMonth = (selectedDate.getMonth()+1 <= 9) ? '0' + (selectedDate.getMonth()+1) : selectedDate.getMonth()+1;

            const currentYear = selectedDate.getFullYear();
            var tempFinal = currentYear + "-" + currentMonth + "-" + currentDate;

            setdateEnd(selectedDate);
            setdateEnds(tempFinal);
        }
    };

    const addEvent = async () => {
        setSpinner(true);

        if (name == null || promoCode == null || dateStarts == null || dateEnds == null || desc == null || minPrice == null || discount == null || maxDiscount == null) {
            Alert.alert(
                'Ooops...',
                'All Field Is Required',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        if (name == null || promoCode == null || dateStarts == null || dateEnds == null || desc == null || minPrice == null || discount == null || maxDiscount == null) {
            Alert.alert(
                'Ooops...',
                'All Field Is Required',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }

        let formData = new FormData();
        formData.append('name_event', name);
        formData.append('status_event', '1');
        formData.append('promo_code', promoCode);
        formData.append('date_start_event', dateStarts);
        formData.append('date_end_event', dateEnds);
        formData.append('desc_event', desc);
        formData.append('min_price', minPrice);
        formData.append('discount_event', discount);
        formData.append('max_discount', maxDiscount);

        console.log(formData);
        var result = await rootStore.addEvent(formData);

        if (result.kind == "ok") {
            ToastAndroid.show('Data Event Has Been Created Succesfully!', ToastAndroid.SHORT);
            props.navigation.replace('profile');
        }
        else if (result.kind == 'wrong') {
            setSpinner(false);
            Alert.alert(
                'Ooops...',
                result.message.toString(),
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
        setSpinner(false);
    }

    useEffect(() => {
    }, [useIsFocused])

    return (
        <View style={{ ...Styles.container, alignItems: 'center' }}>
            <Nav_header
 onPress={() => props.navigation.navigate('notif')}
                back={true}
                title={"Add Event"}
            />

            <ScrollView showsVerticalScrollIndicator={false} style={{ flexDirection: 'column' }}>
                <View style={{ height: 0.02 * deviceWidth }} />
                <View>
                    <Textbox
                        title={"Nama Event"}
                        value={name}
                        editable={true}
                        placeholder={"Nama Event"}
                        keyboardType={"default"}
                        change={values => setName(values)}
                    />
                    <Textbox
                        title={"Promo Code"}
                        value={promoCode}
                        editable={true}
                        placeholder={"Promo Code"}
                        keyboardType={"default"}
                        change={values => setpromoCode(values)}
                    />
                    <TouchableOpacity onPress={() => { setShow(true) }}>

                        <Textbox
                            title={"Date Start"}
                            value={dateStarts}
                            editable={false}
                            placeholder={"Date Start"}
                        />

                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={dateStart}
                                mode={"date"}
                                is24Hour={true}
                                display="default"
                                onChange={onChangeDate}
                            />
                        )}
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { setShow1(true) }}>
                        <Textbox
                            title={"Date End"}
                            value={dateEnds}
                            editable={false}
                            placeholder={"Date End"}
                        />


                        {show1 && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={dateEnd}
                                mode={"date"}
                                is24Hour={true}
                                display="default"
                                onChange={onChangeDate2}
                            />
                        )}
                    </TouchableOpacity>

                    <Textbox
                        title={"Deskripsi"}
                        value={desc}
                        editable={true}
                        placeholder={"Deskripsi"}
                        keyboardType={"default"}
                        change={values => setDesc(values)}
                    />
                    <Textbox
                        title={"Minimum Harga"}
                        value={minPrice}
                        editable={true}
                        placeholder={"Minimum Harga"}
                        keyboardType={"default"}
                        change={values => setminPrice(values)}
                    />
                    <Textbox
                        title={"Jumlah Diskon"}
                        value={discount}
                        editable={true}
                        placeholder={"Jumlah Diskon"}
                        keyboardType={"default"}
                        change={values => setdiscount(values)}
                    />
                    <Textbox
                        title={"Maximum Diskon"}
                        value={maxDiscount}
                        editable={true}
                        placeholder={"Maximum Diskon"}
                        keyboardType={"default"}
                        change={values => setmaxDiscount(values)}
                    />

                    <View style={{ height: 0.02 * deviceWidth }} />
                    <Button_big
                        onPress={() => addEvent()}
                        text={'Add'}
                    />
                </View>
            </ScrollView>
        </View>
    )
}
