import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { ToastAndroid } from "react-native"
import { omit } from "ramda"
import { Api } from "@services/api"



const ApiInstance = new Api();
ApiInstance.setup();

const actionError = (res, model) => {

  if (res.msg == "Unable to authenticate with invalid API key and token.") {
    model.removeCurrentUser();

    ToastAndroid.show("Your session has been expired. Please try to login again.", ToastAndroid.LONG);
  } else {
    var msg = "";
    if (Array.isArray(res.msg) && res.msg.length > 0) {
      res.msg.map((item, i) => {
        msg += item + "\n";
      });
    } else {
      msg = res.msg;
    }

    ToastAndroid.show("Connection to server failed. Try again later.", ToastAndroid.LONG);
  }

}


/**
 * A RootStore model.
 */
// prettier-ignore
export const RootStoreModel = types.model("RootStore").props({
})

  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({
    async login(content) {
      var res = await ApiInstance.login(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getProfile(content) {
      var res = await ApiInstance.getProfile(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async updateProfile(content) {
      var res = await ApiInstance.updateProfile(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getHistoryTrans(content) {
      var res = await ApiInstance.getHistoryTrans(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async addUser(content) {
      var res = await ApiInstance.addUser(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async updateUser(content) {
      var res = await ApiInstance.updateUser(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async deleteCategory(content) {
      var res = await ApiInstance.deleteCategory(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async deleteUser(content) {
      var res = await ApiInstance.deleteUser(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getListUser(content) {
      var res = await ApiInstance.getListUser(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getHistoryLogin(content) {
      var res = await ApiInstance.getHistoryLogin(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async deleteEvent(content) {
      var res = await ApiInstance.deleteEvent(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getListEvent(content) {
      var res = await ApiInstance.getListEvent(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getNotif(content) {
      var res = await ApiInstance.getNotif(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getEvent(content) {
      var res = await ApiInstance.getEvent(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getDetailOrder(content) {
      var res = await ApiInstance.getDetailOrder(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async addCategory(content) {
      var res = await ApiInstance.addCategory(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async addEvent(content) {
      var res = await ApiInstance.addEvent(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async addMenu(content) {
      var res = await ApiInstance.addMenu(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async deleteMenu(content) {
      var res = await ApiInstance.deleteMenu(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async storeOrder(content) {
      var res = await ApiInstance.storeOrder(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async updateMenu(content) {
      var res = await ApiInstance.updateMenu(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getCategory(content) {
      var res = await ApiInstance.getCategory(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getMenu(content) {
      var res = await ApiInstance.getMenu(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
    async getListMenu(content) {
      var res = await ApiInstance.getListMenu(content);

      if (res.kind == "ok") {
      }
      else if (res.kind == 'wrong') {
        console.log(res.message);
      }
      else {
        actionError(res, self);
      }
      return res;
    },
  }))

/**
 * The RootStore instance.
 */
export interface RootStore extends Instance<typeof RootStoreModel> { }

/**
 * The data of a RootStore.
 */
export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> { }
