import { ApisauceInstance, create, ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "./api-problem"
import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import * as Types from "./api.types"
import '../../../global.js'

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.url,
      timeout: this.config.timeout,
      headers: {
        "Accept": "application/json",
        "token": global.bearer_token,
      },
    })
  }

  async login(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/login`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getNotif(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/notification`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getMenu(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/menu/id`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getDetailOrder(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/order/detail`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getEvent(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/event/discount`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async addEvent(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/event/store`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async addCategory(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/category/store`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getListEvent(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/event`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async addMenu(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'multipart/form-data');
    this.apisauce.setHeader('Content-Type', 'multipart/form-data');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/menu/store`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async deleteMenu(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/menu/delete`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async updateMenu(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'multipart/form-data');
    this.apisauce.setHeader('Content-Type', 'multipart/form-data');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/menu/update`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getCategory(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/category`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getProfile(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/profile`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async updateProfile(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'multipart/form-data');
    this.apisauce.setHeader('Content-Type', 'multipart/form-data');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/profile/update`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async storeOrder(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/order/store`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getHistoryTrans(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/myorder`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getHistoryLogin(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/history/login`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async addUser(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'multipart/form-data');
    this.apisauce.setHeader('Content-Type', 'multipart/form-data');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/employee/store`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async updateUser(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'multipart/form-data');
    this.apisauce.setHeader('Content-Type', 'multipart/form-data');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/employee/update`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async deleteCategory(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/category/delete`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async deleteEvent(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/event/delete`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async deleteUser(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/employee/delete`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getListUser(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('bearer_token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/employee`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }
  async getListMenu(data): Promise<Types.GetDefaultResult> {
    this.apisauce.setHeader('Accept', 'application/json');
    this.apisauce.setHeader('Content-Type', 'application/json');
    this.apisauce.setHeader('token', global.bearer_token);

    let response: ApiResponse<any>;
    let path = `/menu`;

    if (data._parts.length == 0) {
      response = await this.apisauce.post(path)
    }
    else {
      response = await this.apisauce.post(path, data)
    }

    if (response.data.status == "error") {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      if (response.data && response.data.status != "error") {
        const result = response.data;
      }
      if (response.data.status == 'success') {
        const resultData: Types.AppDefault = response.data.data;
        return { kind: "ok", data: resultData }
      } else {
        const resultData: Types.AppDefault = response.data.message;
        return { kind: "wrong", message: resultData }
      }

    } catch {
      return { kind: "bad-data", msg: response.data.message }
    }
  }

}
