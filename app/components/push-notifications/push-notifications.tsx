import * as React from "react"
import { Dimensions, View, Image, Text, TouchableOpacity, Platform } from "react-native"
import { Styles, Images } from "@theme"
import PushNotification, {Importance}  from 'react-native-push-notification'
import { useNavigation } from "@react-navigation/native";

// FirebaseApp.initializeApp();
PushNotification.configure({
    // (required) Called when a remote or local notification is opened or received
    onNotification: function (notification) {
        console.log('LOCAL NOTIFICATION ==>', notification)
        if (notification.userInteraction) {
            // Handle notification click
        }
    },
    popInitialNotification: true,
    // requestPermissions: Platform.OS === 'ios'
    // requestPermissions: true
})


PushNotification.createChannel(
    {
      channelId: "1", // (required)
      channelName: "My channel", // (required)
      channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
      playSound: false, // (optional) default: true
      soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
      importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
      vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
    },
    (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
  );


export function PushNotifications(title, desc) {

    console.log(title);
    console.log(desc);
    PushNotification.localNotification({
        channelId: "1", // (required)
        channelName: "My channel", // (required)
        autoCancel: true,
        bigText: desc,
        subText: title,
        title: title,
        message: desc,
        vibrate: true,
        vibration: 300,
        playSound: true,
        soundName: 'default',
    })
}
