import * as React from "react"
import { Dimensions, View, Image, Text, TouchableOpacity } from "react-native"
import { Styles, MainStyle, Images } from "@theme"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faBell } from "@fortawesome/free-solid-svg-icons"

const deviceWidth = Dimensions.get("window").width;

export function Nav_header(props: HeaderProps) {

    const {
        preset = "primary",
        tx,
        text,
        style: styleOverride,
        textStyle: textStyleOverride,
        children,
        ...rest
    } = props

    const content = children || <Text tx={tx} text={text} />

    return (
        <View style={{ ...Styles.navheader_view }}>

            {/* {(props.back) ?
            <TouchableOpacity onPress={props.onPress}>

                <Image
                    style={{
                        ...Styles.size_20,
                        marginLeft: 0.055*deviceWidth,
                    }}
                    source={Images.icon_arrow_left}
                />

            </TouchableOpacity>
        :
            <View style={{width: 0.11*deviceWidth}} />
        } */}

            <Text
                allowFontScaling={false}
                numberOfLines={1}
                style={{ ...Styles.navheader_text, width: 0.5 * deviceWidth, textAlign: 'right' }}
            >{props.title}</Text>

            <View style={{ width: 0.4 * deviceWidth, alignItems: 'flex-end' }}>
                <TouchableOpacity
                onPress={props.onPress}
                style={{ ...Styles.size_30, ...MainStyle.bgcolor_green, borderRadius: 25, justifyContent: 'center', alignItems: 'center' }}>
                    <FontAwesomeIcon
                        style={{ ...Styles.size_30, color: 'white' }}
                        icon={faBell} />
                </TouchableOpacity>
            </View>

        </View>
    )
}
