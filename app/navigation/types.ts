export type RootParamList = {
  primaryStack: undefined
}

export type PrimaryParamList = {
  welcome: undefined
  login: undefined
  home: undefined
  bootstrap: undefined
  order: undefined
  activity: undefined
  profile: undefined
  profile_user_add: undefined
  profile_user_edit: undefined
  profile_edit: undefined
  list_user: undefined
  menu_edit: undefined
  menu_add: undefined
  list_event: undefined
  event_add: undefined
  notif: undefined
  list_category: undefined
  category_add: undefined
  order_detail: undefined
}
