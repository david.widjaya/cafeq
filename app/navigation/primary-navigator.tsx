import React from "react"

import { createNativeStackNavigator } from "@react-navigation/native-stack"
import {
  WelcomeScreen,
  LoginScreen,
  HomeScreen,
  BootstrapScreen,
  OrderScreen,
  ActivityScreen,
  ProfileScreen,
  ListUserScreen,
  ProfileEditScreen,
  ProfileAddUserScreen,
  ProfileEditUserScreen,
  MenuEditScreen,
  MenuAddScreen,
  ListEventScreen,
  EventAddScreen,
  NotifScreen,
  ListCategoryScreen,
  CategoryAddScreen,
  OrderDetailScreen,
} from "../screens"

import { PrimaryParamList } from "./types"

const Stack = createNativeStackNavigator<PrimaryParamList>()

export function PrimaryNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
      }}
    >
      {/* <Stack.Screen name="welcome" component={WelcomeScreen} /> */}
      <Stack.Screen name="login" component={LoginScreen} />
      <Stack.Screen name="home" component={HomeScreen} />
      <Stack.Screen name="bootstrap" component={BootstrapScreen} />
      <Stack.Screen name="activity" component={ActivityScreen} />
      <Stack.Screen name="profile" component={ProfileScreen} />
      <Stack.Screen name="order" component={OrderScreen} />
      <Stack.Screen name="profile_user_add" component={ProfileAddUserScreen} />
      <Stack.Screen name="profile_user_edit" component={ProfileEditUserScreen} />
      <Stack.Screen name="profile_edit" component={ProfileEditScreen} />
      <Stack.Screen name="list_user" component={ListUserScreen} />
      <Stack.Screen name="menu_edit" component={MenuEditScreen} />
      <Stack.Screen name="menu_add" component={MenuAddScreen} />
      <Stack.Screen name="list_event" component={ListEventScreen} />
      <Stack.Screen name="event_add" component={EventAddScreen} />
      <Stack.Screen name="notif" component={NotifScreen} />
      <Stack.Screen name="list_category" component={ListCategoryScreen} />
      <Stack.Screen name="category_add" component={CategoryAddScreen} />
      <Stack.Screen name="order_detail" component={OrderDetailScreen} />
    </Stack.Navigator>
  )
}

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 */
export const exitRoutes: string[] = ["login"]
